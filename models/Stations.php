<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "stations".
 *
 * @property int $id
 * @property string $location Местоположение
 * @property int $user_id Менеджер
 * @property string $coordinate_x Координата x
 * @property string $coordinate_y Координата y
 *
 * @property Rents[] $rents
 * @property Rents[] $rents0
 * @property Scooters[] $scooters
 * @property Users $user
 */
class Stations extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['location', 'user_id'], 'required'],
            [['location'], 'string'],
            [['user_id'], 'integer'],
            [['coordinate_x', 'coordinate_y'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'location' => 'Местоположение',
            'user_id' => 'Менеджер',
            'coordinate_x' => 'Координата x',
            'coordinate_y' => 'Координата y',
        ];
    }

    public function beforeDelete()
    {
        $scooters = Scooters::find()->where(['station_id' => $this->id])->all();
        foreach ($scooters as $value) {
            $value->station_id = null;
            $value->save();
        }

        $rents = Rents::find()->where(['begin_station' => $this->id])->all();
        foreach ($rents as $value) {
            $value->delete();
        }

        $rents = Rents::find()->where(['end_station' => $this->id])->all();
        foreach ($rents as $value) {
            $value->delete();
        }
        
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRents()
    {
        return $this->hasMany(Rents::className(), ['begin_station' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRents0()
    {
        return $this->hasMany(Rents::className(), ['end_station' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScooters()
    {
        return $this->hasMany(Scooters::className(), ['station_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    public function getUsersList()
    {
        $users = Users::find()->all();
        return ArrayHelper::map($users, 'id', 'fio');
    }

    public function getScootersList()
    {
        $query = Scooters::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);

        $query->andFilterWhere(['station_id' => $this->id]);
        return $dataProvider;
    }
}
