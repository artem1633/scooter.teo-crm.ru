<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tariffs".
 *
 * @property int $id
 * @property string $name Наименование тарифа
 * @property double $cost Стоимость
 * @property int $type Тип
 *
 * @property Rents[] $rents
 * @property WeeklyCost[] $weeklyCosts
 */
class Tariffs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tariffs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cost', 'name'], 'required'],
            [['cost'], 'number'],
            [['type'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование тарифа',
            'cost' => 'Стоимость (по умолчанию)',
            'type' => 'Тип',
        ];
    }

    public function beforeDelete()
    {
        $rents = Rents::find()->where(['tariff_id' => $this->id])->all();
        foreach ($rents as $value) {
            $value->delete();
        }

        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRents()
    {
        return $this->hasMany(Rents::className(), ['tariff_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWeeklyCosts()
    {
        return $this->hasMany(WeeklyCost::className(), ['tariff_id' => 'id']);
    }

    public function getCost($tariff_id, $minutes, $begin_date, $tariff_cost)
    {
        $days = []; $total_summa = 0;
        if($tariff_id == 1) {
            $end = strtotime($begin_date) + 60 * $minutes;
            for ($i = strtotime($begin_date); $i < $end; $i = $i + 60) {
                $days [] = [
                    'week_day' => date('N', $i), //день недели
                    'datetime' => date('H:i:00', $i), //дата и время
                    'price' => 0, //цена
                ];                
            }
            $datas = WeeklyCost::find()->where(['tariff_id' => $tariff_id])->all();
            foreach ($days as $day) {
                $finded = 0;
                foreach ($datas as $data) {
                    if($data->week_day == $day['week_day'] && $data->begin_time <= $day['datetime'] && $day['datetime'] < $data->end_time){
                        $total_summa += $data->cost;
                        $finded = 1;
                        break;
                    }
                }
                if($finded == 0){
                    $total_summa += $tariff_cost;
                }
            }
            return $total_summa;
        }

        if($tariff_id == 2) {
            $end = strtotime($begin_date) + 60 * $minutes;
            for ($i = strtotime($begin_date); $i < $end; $i = $i + 60) {
                $days [] = [
                    'week_day' => date('N', $i), //день недели
                    'datetime' => date('H:i:00', $i), //дата и время
                    'price' => 0, //цена
                ];                
            }
            $datas = WeeklyCost::find()->where(['tariff_id' => $tariff_id])->all();
            foreach ($days as $day) {
                $finded = 0;
                foreach ($datas as $data) {
                    if($data->week_day == $day['week_day'] && $data->begin_time <= $day['datetime'] && $day['datetime'] < $data->end_time){
                        $total_summa += $data->cost / 60;
                        $finded = 1;
                        break;
                    }
                }
                if($finded == 0){
                    $total_summa += $tariff_cost / 60;
                }
            }
            return $total_summa;
        }
    }
}
