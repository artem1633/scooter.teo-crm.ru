<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "rents".
 *
 * @property int $id
 * @property int $begin_station Станция начала проката
 * @property int $end_station Станция окончания проката
 * @property int $scooter_id Самоката
 * @property string $begin_date Время начала поездки
 * @property string $end_date Время окончания поездки
 * @property int $client_id Клиента
 * @property int $tariff_id Тарифа
 * @property double $summa Сумма
 * @property double $trip_duration Длительности поездки
 * @property int $payment_status Факт оплаты
 * @property string $payment_time Дата и время оплаты
 * @property string $comment Комментария
 * @property double $run_length Пробег
 * @property double $penalty_sum Сумма штрафа
 * @property int $status Статус
 * @property string $factual_datetime Фактическое время возврата
 * @property int $promo_id Промо-код
 *
 * @property RentPenalty[] $rentPenalties
 * @property Stations $beginStation
 * @property Clients $client
 * @property Stations $endStation
 * @property PromoCodes $promo
 * @property Scooters $scooter
 * @property Tariffs $tariff
 */
class Rents extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rents';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['begin_station', 'end_station', 'scooter_id', 'client_id', 'tariff_id', 'payment_status', 'status'], 'integer'],
            [['begin_date', 'end_date', 'payment_time', 'factual_datetime'], 'safe'],
            [['summa', 'trip_duration', 'run_length', 'penalty_sum'], 'number'],
            [['comment'], 'string'],
            [['promo_code'], 'string', 'max' => 255],
            [['tariff_id', 'client_id', 'begin_station', 'scooter_id', 'begin_date'], 'required'],
            [['begin_station'], 'exist', 'skipOnError' => true, 'targetClass' => Stations::className(), 'targetAttribute' => ['begin_station' => 'id']],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clients::className(), 'targetAttribute' => ['client_id' => 'id']],
            [['end_station'], 'exist', 'skipOnError' => true, 'targetClass' => Stations::className(), 'targetAttribute' => ['end_station' => 'id']],
            [['scooter_id'], 'exist', 'skipOnError' => true, 'targetClass' => Scooters::className(), 'targetAttribute' => ['scooter_id' => 'id']],
            [['tariff_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tariffs::className(), 'targetAttribute' => ['tariff_id' => 'id']],
            [['promo_id'], 'exist', 'skipOnError' => true, 'targetClass' => PromoCodes::className(), 'targetAttribute' => ['promo_id' => 'id']],
            //['begin_date', 'validateBeginDate'],
            //['trip_duration', 'validateTrip'],
            /*[['trip_duration'], 'required', 'when' => function() {
                   if($this->tariff_id == 2) return TRUE;
                   else return FALSE;
            }],*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'begin_station' => 'Станция начала проката',
            'end_station' => 'Станция окончания проката',
            'scooter_id' => 'Самокат',
            'begin_date' => 'Время началы поездки',
            'end_date' => 'Время окончания поездки',
            'client_id' => 'Клиент',
            'tariff_id' => 'Тариф',
            'summa' => 'Стоимость оплаты',
            'trip_duration' => 'Длительности поездки',
            'payment_status' => 'Факт оплаты',
            'payment_time' => 'Дата и время оплаты',
            'comment' => 'Комментария',
            'run_length' => 'Пробег',
            'penalty_sum' => 'Сумма штрафа',
            'status' => 'Статус аренды',
            'factual_datetime' => 'Фактическое время возврата',
            'promo_id' => 'Промо-код',
            'promo_code' => 'Промо-код',
        ];
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord)
        {
            //$this->summa = $this->trip_duration * $this->tariff->cost;
            $this->run_length = 0;
            $this->status = 0;
            if(strtotime($this->begin_date) < strtotime(date('Y-m-d H:i:00'))) $this->begin_date = date('Y-m-d H:i:00');

            if($this->tariff_id == 2){
                $this->end_date = date('Y-m-d H:i', strtotime($this->trip_duration . " hour", strtotime($this->begin_date)));
            }
        }
        if($this->begin_date !== null) $this->begin_date = date('Y-m-d H:i:00', strtotime($this->begin_date));
        if($this->end_date !== null) $this->end_date = date('Y-m-d H:i:00', strtotime($this->end_date));
        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        $rents = RentPenalty::find()->where(['rent_id' => $this->id])->all();
        foreach ($rents as $value) {
            $value->delete();
        }

        return parent::beforeDelete();
    }

    public function afterFind()
    {
        if($this->end_date !== null) $this->end_date = date('H:i d.m.Y', strtotime($this->end_date));
        parent::afterFind();
    }

    /*public function validateBeginDate($attribute)
    { 
        if($this->isNewRecord){
            $begin_date = $this->begin_date;
            if(strtotime($begin_date) < strtotime(date('Y-m-d H:i:00'))) $this->addError($attribute, '«Время началы поездки» не можеть быть меньше текущего');            
        }
    }*/

    public function setValues()
    {
        $scooter = Scooters::find()->where(['id' => $this->scooter_id])->one();
        if($scooter !== null){
            $scooter->status = Scooters::STATUS_BUSY;
            $scooter->client_id = $this->client_id;
            $scooter->save();
        }
        $promo = PromoCodes::find()->where(['code' => $this->promo_code, 'status' => 2])->one();
        if($promo !== null){
            $this->promo_id = $promo->id;
            if($this->tariff_id == 2) {
                $minutes = $this->trip_duration * 60;
                if($promo->type == 1){
                    $minutes = $minutes - $promo->time;
                    if($minutes > 0) $this->summa = Tariffs::getCost($this->tariff_id, $minutes, $this->begin_date, $this->tariff->cost);
                    else $this->summa = 0;
                }
                else{
                    $this->summa = Tariffs::getCost($this->tariff_id, $minutes, $this->begin_date, $this->tariff->cost);
                    $summa = $promo->summa;
                    if($summa > $this->summa) $this->summa = 0;
                    else $this->summa -= $summa;
                }
            }
            $this->save();
            $promo->status = 3;
            $promo->save();
        }
        else{
            if($this->tariff_id == 2) {
                $minutes = $this->trip_duration * 60;
                $this->summa = Tariffs::getCost($this->tariff_id, $minutes, $this->begin_date, $this->tariff->cost);
                $this->save();
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRentPenalties()
    {
        return $this->hasMany(RentPenalty::className(), ['rent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBeginStation()
    {
        return $this->hasOne(Stations::className(), ['id' => 'begin_station']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEndStation()
    {
        return $this->hasOne(Stations::className(), ['id' => 'end_station']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScooter()
    {
        return $this->hasOne(Scooters::className(), ['id' => 'scooter_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(Tariffs::className(), ['id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromo()
    {
        return $this->hasOne(PromoCodes::className(), ['id' => 'promo_id']);
    }

    public function getStationsList()
    {
        $stations = Stations::find()->all();
        return ArrayHelper::map($stations, 'id', 'location');
    }

    public function getScootersList($station_id = null)
    {
        $scooters = Scooters::find()->where(['station_id' => $station_id, 'status' => Scooters::STATUS_VACANT])->all();
        $result = [];
        foreach ($scooters as $value) {
            $result [] = [
                'id' => $value->id,
                'title' => 'Id = ' . $value->id. '; QR-код = '. $value->qr_code,
            ];
        }

        return ArrayHelper::map($result, 'id', 'title');
    }

    public function getClientsList()
    {
        $clients = Clients::find()->all();
        $result = [];
        foreach ($clients as $value) {
            $result [] = [
                'id' => $value->id,
                'title' => $value->surname. ' '. $value->name. ' '. $value->middle_name,
            ];
        }

        return ArrayHelper::map($result,'id', 'title');
    }

    public function getTariffList()
    {
        $tariff = Tariffs::find()->all();
        return ArrayHelper::map($tariff, 'id', 'name');
    }

    public function getPaymentStatusList()
    {
        return [
            0 => 'Оплата по окончанию',
            1 => 'Оплачено'
        ];
    }

    public function getPaymentStatusDescription()
    {
        if($this->payment_status == 0) return 'Оплата по окончанию';
        if($this->payment_status == 1) return 'Оплачено';
    }

    public function getStatusList()
    {
        return [
            0 => 'Не завершено',
            1 => 'Завершено'
        ];
    }

    public function getStatusDescription()
    {
        if($this->status == 0) return 'Не завершено';
        if($this->status == 1) return 'Завершено';
    }

    public function saveValues()
    {
        $scooter = Scooters::findOne($this->scooter_id);
        $scooter->status = Scooters::STATUS_VACANT;
        $scooter->run += $this->run_length;
        $scooter->station_id = $this->end_station;
        $scooter->client_id = null;
        $scooter->save();
        $this->status = 1;
        $this->payment_time = date('Y-m-d H:i:s');
        if($this->tariff_id == 1) {
            $this->end_date = date('Y-m-d H:i:00');
            $used_time = strtotime($this->end_date) - strtotime($this->begin_date);
            $minutes = $used_time / 60;
            $this->trip_duration = $minutes;

            if($this->promo_id != null){
                if($this->promo->type == 1){
                    $minutes = $minutes - $this->promo->time;
                    if($minutes > 0){
                        //$summa = $minutes * $this->tariff->cost;
                        $this->summa = Tariffs::getCost($this->tariff_id, $minutes, $this->begin_date, $this->tariff->cost);
                    }
                    else $this->summa = 0;
                }
                else{
                    //$summa = $minutes * $this->tariff->cost;
                    $summa = Tariffs::getCost($this->tariff_id, $minutes, $this->begin_date, $this->tariff->cost);
                    $summa -= $this->promo->summa;
                    if($summa > 0) $this->summa = $summa;
                    else $this->summa = 0;
                }                
            }
            else{
                $this->summa = Tariffs::getCost($this->tariff_id, $minutes, $this->begin_date, $this->tariff->cost);
            }
        }
        $this->factual_datetime = date('Y-m-d H:i:00');
        if($this->end_date === null) $this->end_date = $this->factual_datetime;
        $this->save();
    }

    public function promoDescription()
    {
        if($this->promo_id != null){
            if($this->promo->type == 1) $string = 'Тип промокода: По времени поездки, Время (мин): '.$this->promo->time;
            else $string = 'Тип промокода: По сумме, Сумма: '.$this->promo->summa;
            return $string;
        }
        else return 'Промо код не найдено';
    }

    public function summaDescription()
    {
        if($this->status == 1) return $this->summa;
        else {
            if($this->tariff_id == 2) return $this->summa;
            if($this->tariff_id == 1) {
                $end_date = date('Y-m-d H:i:00');
                $used_time = strtotime($end_date) - strtotime($this->begin_date);
                $minutes = $used_time / 60;
                $total_sum = 0;

                if($this->promo_id != null){
                    if($this->promo->type == 1){
                        $minutes = $minutes - $this->promo->time;
                        if($minutes > 0){
                            $total_sum = Tariffs::getCost($this->tariff_id, $minutes, $this->begin_date, $this->tariff->cost);
                        }
                        else $total_sum = 0;
                    }
                    else{
                        $summa = Tariffs::getCost($this->tariff_id, $minutes, $this->begin_date, $this->tariff->cost);
                        $summa -= $this->promo->summa;
                        if($summa > 0) $total_sum = $summa;
                        else $total_sum = 0;
                    }                
                }
                else{
                    $total_sum = Tariffs::getCost($this->tariff_id, $minutes, $this->begin_date, $this->tariff->cost);
                }
                return $total_sum;
            }
        }
    }
}
