<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Rents;

/**
 * RentsSearch represents the model behind the search form about `app\models\Rents`.
 */
class RentsSearch extends Rents
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'begin_station', 'end_station', 'scooter_id', 'client_id', 'tariff_id', 'status', 'promo_id'], 'integer'],
            [['begin_date', 'end_date', 'payment_status', 'payment_time', 'comment', 'promo_code'], 'safe'],
            [['summa', 'trip_duration', 'run_length'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rents::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'begin_station' => $this->begin_station,
            'end_station' => $this->end_station,
            'scooter_id' => $this->scooter_id,
            'begin_date' => $this->begin_date,
            'end_date' => $this->end_date,
            'client_id' => $this->client_id,
            'tariff_id' => $this->tariff_id,
            'summa' => $this->summa,
            'trip_duration' => $this->trip_duration,
            'payment_time' => $this->payment_time,
            'run_length' => $this->run_length,
            'status' => $this->status,
            'promo_id' => $this->promo_id,
            'promo_code' => $this->promo_code,
        ]);

        $query->andFilterWhere(['like', 'payment_status', $this->payment_status])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
