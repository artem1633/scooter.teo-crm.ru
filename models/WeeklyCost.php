<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "weekly_cost".
 *
 * @property int $id
 * @property int $week_day День недели
 * @property string $begin_time Время начала
 * @property string $end_time Время окончание
 * @property double $cost Стоимость
 * @property int $tariff_id Тариф
 *
 * @property Tariffs $tariff
 */
class WeeklyCost extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'weekly_cost';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['week_day', 'tariff_id'], 'integer'],
            [['begin_time', 'end_time'], 'safe'],
            [['cost'], 'number'],
            [['begin_time', 'end_time', 'cost'], 'required'],
            [['tariff_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tariffs::className(), 'targetAttribute' => ['tariff_id' => 'id']],
            ['end_time', 'validateEnd'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'week_day' => 'День недели',
            'begin_time' => 'Время начала',
            'end_time' => 'Время окончание',
            'cost' => 'Стоимость',
            'tariff_id' => 'Тариф',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(Tariffs::className(), ['id' => 'tariff_id']);
    }

    public function getWeekDay()
    {
        return [
            1 => 'Понедельник',
            2 => 'Вторник',
            3 => 'Среда',
            4 => 'Четверг',
            5 => 'Пятница',
            6 => 'Суббота',
            7 => 'Воскресенье',
        ];
    }

    public function getCostDescription()
    {
        if($this->tariff_id == 1) return $this->cost . ' руб/мин';
        if($this->tariff_id == 2) return $this->cost . ' руб/час';
    }

    public function validateEnd($attribute)
    { 
        if(strtotime($this->begin_time) > strtotime($this->end_time)) $this->addError($attribute, '«Время окончание» не можеть быть меньше «Время началы»');      
    }
}
