<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "clients".
 *
 * @property int $id
 * @property string $phone Телефон
 * @property string $surname Фамилия
 * @property string $name Имя
 * @property string $middle_name Отчество
 * @property string $birthday Дата рождения
 * @property double $payed_sum Оплаченная сумма всего
 * @property double $rental_hour Кол-во часов аренды
 * @property double $number_of_trips Кол-во поездок
 * @property string $passport_number Серия и номер
 * @property string $passport_issue Кем выдан паспорт
 * @property string $unit_code Код подразделения
 * @property string $passport_photo Фотография паспорта
 * @property int $agree Я согласен
 * @property int $status Статус
 * @property string $ip IP
 * @property string $model Модель устройства
 * @property string $datetime_agree Дата и время
 *
 * @property Rents[] $rents
 * @property Scooters[] $scooters
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    
    public $file;

    const STATUS_ACCESS = '1';
    const STATUS_NO_ACCESS = '0';

    public static function tableName()
    {
        return 'clients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['birthday', 'datetime_agree'], 'safe'],
            [['payed_sum', 'rental_hour', 'number_of_trips'], 'number'],
            [['surname', 'name', 'middle_name', 'passport_number', 'passport_issue', 'unit_code'], 'required'],
            [['passport_issue', 'sms_text'], 'string'],
            [['agree', 'status', 'passport_checked'], 'integer'],
            [['phone', 'surname', 'name', 'middle_name', 'passport_number', 'unit_code', 'passport_photo', 'ip', 'model'], 'string', 'max' => 255],
            /*[['surname','middle_name',],  'match' , 'pattern'=> '/^[А-Яа-я\s-]+$/u', 'message'=> 'Только русские буквы, дефис, пробел'],
            ['name',  'match' , 'pattern'=> '/^[А-Яа-я-]+$/u', 'message'=> 'Только русские буквы и дефис'],*/
            ['passport_number', 'validatePassportNumber'],
            ['unit_code', 'validateUnitCode'],
            ['phone', 'validateTelephoneNumber'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg',], 
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Телефон',
            'surname' => 'Фамилия',
            'name' => 'Имя',
            'middle_name' => 'Отчество',
            'birthday' => 'Дата рождения',
            'passport_number' => 'Серия и номер паспорта',
            'passport_issue' => 'Кем выдан паспорт',
            'unit_code' => 'Код подразделения',
            'passport_photo' => 'Фотография паспорта',
            'passport_checked' => 'Паспорт',

            'payed_sum' => 'Оплаченная сумма всего',
            'rental_hour' => 'Кол-во часов аренды',
            'number_of_trips' => 'Кол-во поездок',
            'agree' => 'Я согласен',
            'status' => 'Статус',
            'ip' => 'IP',
            'model' => 'Модель устройства',
            'datetime_agree' => 'Дата подтверждение',
            'sms_text' => 'Текст смса',

            'file' => 'Фотография паспорта',
        ];
    }

    public function validatePassportNumber($attribute)
    { 
        $passport_number = $this->passport_number;
        $strlen = strlen( $passport_number );
        $numeric = 0; 
        for( $i = 0; $i <= $strlen; $i++ ) 
        {
            $char = substr( $passport_number, $i, 1 );             
            if(ord($char) > 47 && ord($char) < 58) $numeric++;            
        }
        if($numeric != 10) $this->addError($attribute, 'Вводите полностью полю «Серия и номер паспорта»');
    }

    public function validateTelephoneNumber($attribute)
    { 
        $phone = $this->phone;
        $strlen = strlen( $phone );
        $numeric = 0; 
        for( $i = 0; $i <= $strlen; $i++ ) 
        {
            $char = substr( $phone, $i, 1 );             
            if(ord($char) > 47 && ord($char) < 58) $numeric++;            
        }
        if($numeric != 11) $this->addError($attribute, 'Вводите полностью полю «Номер мобильного телефона»');
    }

    public function validateUnitCode($attribute)
    { 
        $unit_code = $this->unit_code;
        $strlen = strlen( $unit_code );
        $numeric = 0; 
        for( $i = 0; $i <= $strlen; $i++ ) 
        {
            $char = substr( $unit_code, $i, 1 );             
            if(ord($char) > 47 && ord($char) < 58) $numeric++;            
        }
        if($numeric != 6) $this->addError($attribute, 'Вводите полностью полю «Код подразделения»');
    }

    public function upload()
    {
        $this->file = UploadedFile::getInstance($this, 'file');
        if(!empty($this->file)) {
            $this->file->saveAs('uploads/clients/'.$this->id.'.'.$this->file->extension);
            /*$this->passport_photo = $this->id.'.'.$this->file->extension;*/
            Yii::$app->db->createCommand()->update('clients', 
                [ 'passport_photo' => $this->id.'.'.$this->file->extension ], 
                [ 'id' => $this->id ])
            ->execute();
        }
    }

    public function afterFind()
    {
        if($this->birthday !== null) $this->birthday = date('d.m.Y', strtotime($this->birthday));
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord)
        {
            $this->status = self::STATUS_NO_ACCESS;
            $this->agree = 0;
            $this->passport_checked = 0;
        }
        if($this->birthday !== null) $this->birthday = date('Y-m-d', strtotime($this->birthday));
        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        $rents = Rents::find()->where(['client_id' => $this->id])->all();
        foreach ($rents as $value) {
            $value->delete();
        }

        $scooters = Scooters::find()->where(['client_id' => $this->id])->all();
        foreach ($scooters as $value) {
            $value->delete();
        }

        unlink(getcwd().'/uploads/clients/'.$this->passport_photo);
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRents()
    {
        return $this->hasMany(Rents::className(), ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScooters()
    {
        return $this->hasMany(Scooters::className(), ['client_id' => 'id']);
    }

    public function getStatusDescription()
    {        
        if(self::STATUS_NO_ACCESS == $this->status) return 'Не подтверждён';
        if(self::STATUS_ACCESS == $this->status) return 'Подтверждён';       
    }

    public function getPassportDescription()
    {
        if(0 == $this->passport_checked) return 'Пасспорт не проверен';
        if(1 == $this->passport_checked) return 'Пасспорт проверен';
    }

    public function sendSms($telephone, $text)
    {
        $smsLogin = Settings::find()->where(['key' => 'login_sms_aero'])->one();
        $smsParol = Settings::find()->where(['key' => 'token_sms_aero'])->one();
        /*$api_url = "https://{$smsLogin->text}:{$smsParol->text}@gate.smsaero.ru/v2/auth";
        $result = file_get_contents($api_url, false);
        $response = json_decode($result);*/
        $api_url = "https://{$smsLogin->text}:{$smsParol->text}@gate.smsaero.ru/v2/sms/send?number={$telephone}&text={$text}&sign=SMS Aero&channel=DIRECT";
        $result = file_get_contents($api_url, false);
        $response = json_decode($result);

        /*$this->sms_text = $response . '<br>' . $api_url;
        $this->save();*/
    }

    public function getPhone($string)
    {
        $strlen = strlen( $string );
        $numeric = ''; 
        for( $i = 0; $i <= $strlen; $i++ ) 
        {
            $char = substr( $string, $i, 1 );             
            if(ord($char) > 47 && ord($char) < 58) $numeric .= $char;            
        }
        return $numeric;
    }
}
