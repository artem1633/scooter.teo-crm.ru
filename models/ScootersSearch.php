<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Scooters;

/**
 * ScootersSearch represents the model behind the search form about `app\models\Scooters`.
 */
class ScootersSearch extends Scooters
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'state', 'client_id', 'station_id'], 'integer'],
            [['qr_code', 'coordinate_x', 'coordinate_y'], 'safe'],
            [['run'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Scooters::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'run' => $this->run,
            'state' => $this->state,
            'client_id' => $this->client_id,
            'station_id' => $this->station_id,
        ]);

        $query->andFilterWhere(['like', 'qr_code', $this->qr_code])
            ->andFilterWhere(['like', 'coordinate_x', $this->coordinate_x])
            ->andFilterWhere(['like', 'coordinate_y', $this->coordinate_y]);

        return $dataProvider;
    }
}
