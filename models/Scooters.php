<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "scooters".
 *
 * @property int $id
 * @property string $qr_code QR-код
 * @property int $status Статус
 * @property double $run Пробег в км
 * @property int $state Состояние
 * @property int $client_id Клиент
 * @property string $coordinate_x Координата x
 * @property string $coordinate_y Координата y
 * @property int $station_id Скутер
 *
 * @property Rents[] $rents
 * @property Clients $client
 * @property Stations $station
 */
class Scooters extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'scooters';
    }

    const STATUS_VACANT = '1';
    const STATUS_BUSY = '2';

    const STATE_IN_WORK = '1';
    const STATE_IN_REPAIR = '2';

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['qr_code'], 'required'],
            [['qr_code'], 'unique'],
            [['status', 'state', 'client_id', 'station_id'], 'integer'],
            [['run'], 'number'],
            [['qr_code', 'coordinate_x', 'coordinate_y'], 'string', 'max' => 255],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clients::className(), 'targetAttribute' => ['client_id' => 'id']],
            [['station_id'], 'exist', 'skipOnError' => true, 'targetClass' => Stations::className(), 'targetAttribute' => ['station_id' => 'id']],
            [['station_id'], 'required', 'when' => function() {
                   if($this->isNewRecord) return TRUE;
                   else return FALSE;
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'qr_code' => 'QR-код',
            'status' => 'Статус',
            'run' => 'Пробег в км',
            'state' => 'Состояние',
            'client_id' => 'Текущий клиент',
            'coordinate_x' => 'Координата x',
            'coordinate_y' => 'Координата y',
            'station_id' => 'Текущая станция',
        ];
    }

    public function beforeDelete()
    {
        $rents = Rents::find()->where(['scooter_id' => $this->id])->all();
        foreach ($rents as $value) {
            $value->delete();
        }
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRents()
    {
        return $this->hasMany(Rents::className(), ['scooter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStation()
    {
        return $this->hasOne(Stations::className(), ['id' => 'station_id']);
    }

    public function getStatusDescription()
    {
        if(self::STATUS_VACANT == $this->status) return 'Свободен';
        if(self::STATUS_BUSY == $this->status) return 'Занят';
    }

    public function getStatusList()
    { 
        return [
            self::STATUS_VACANT => 'Свободен',
            self::STATUS_BUSY => 'Занят',
        ];
    }

    public function getStateDescription()
    {
        if(self::STATE_IN_WORK == $this->status) return 'В работе';
        if(self::STATE_IN_REPAIR == $this->status) return 'В ремонте';
    }

    public function getStateList()
    { 
        return [
            self::STATE_IN_WORK => 'В работе',
            self::STATE_IN_REPAIR => 'В ремонте',
        ];
    }

    public function getClientsList()
    {
        $clients = Clients::find()->all();
        $result = [];
        foreach ($clients as $value) {
            $result [] = [
                'id' => $value->id,
                'title' => $value->surname. ' '. $value->name. ' '. $value->middle_name,
            ];
        }

        return ArrayHelper::map($result,'id', 'title');
    }

    public function getStationList()
    {
        $stations = Stations::find()->all();
        return ArrayHelper::map($stations, 'id', 'location');
    }
}
