<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Clients;

/**
 * ClientsSearch represents the model behind the search form about `app\models\Clients`.
 */
class ClientsSearch extends Clients
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'passport_checked'], 'integer'],
            [['phone', 'surname', 'name', 'middle_name', 'birthday', 'passport_number', 'passport_issue', 'unit_code', 'passport_photo', 'agree', 'ip', 'model', 'datetime_agree'], 'safe'],
            [['payed_sum', 'rental_hour', 'number_of_trips'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Clients::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'birthday' => $this->birthday,
            'payed_sum' => $this->payed_sum,
            'rental_hour' => $this->rental_hour,
            'number_of_trips' => $this->number_of_trips,
            'status' => $this->status,
            'datetime_agree' => $this->datetime_agree,
            'passport_checked' => $this->passport_checked,
        ]);

        $query->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'middle_name', $this->middle_name])
            ->andFilterWhere(['like', 'passport_number', $this->passport_number])
            ->andFilterWhere(['like', 'passport_issue', $this->passport_issue])
            ->andFilterWhere(['like', 'unit_code', $this->unit_code])
            ->andFilterWhere(['like', 'passport_photo', $this->passport_photo])
            ->andFilterWhere(['like', 'agree', $this->agree])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'model', $this->model]);

        return $dataProvider;
    }
}
