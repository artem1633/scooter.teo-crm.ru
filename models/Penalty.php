<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "penalty".
 *
 * @property int $id
 * @property int $category Категория
 * @property double $price Стоимость
 * @property int $damage Повреждения
 *
 * @property RentPenalty[] $rentPenalties
 */
class Penalty extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'penalty';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category', 'damage'], 'integer'],
            [['price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category' => 'Категория',
            'price' => 'Стоимость',
            'damage' => 'Тип повреждений',
        ];
    }

    public function getCategoriesList()
    {
        return [
            1 => 'Невозможно провести оплату по карте',
            2 => 'Нарушение правил пользования',
            3 => 'Повреждения',
        ];
    }

    public function getDamageList()
    {
        return [
            1 => 'Сломанный руль',
            2 => 'Сломанный складной механизм',
            3 => 'Повреждён или сломан курок тормоза',
            4 => 'Повреждён или сломан курок газа',
            5 => 'Не работает панель управления',
            6 => 'Повреждение переднего колеса',
            7 => 'Повреждение заднего колеса',
            8 => 'Повреждение платформы для ног',
            9 => 'Повреждение или утрата доп. аккумулятора',
            10 => 'Утрата самоката или повреждение более 80%',
        ];
    }

    public function beforeDelete()
    {
        $rent_penalty = RentPenalty::find()->where(['penalty_id' => $this->id])->all();
        foreach ($rent_penalty as $value) {
            $value->delete();
        }

        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRentPenalties()
    {
        return $this->hasMany(RentPenalty::className(), ['penalty_id' => 'id']);
    }
}
