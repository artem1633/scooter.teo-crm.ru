<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "promo_codes".
 *
 * @property int $id
 * @property string $code Код
 * @property int $type Тип промокода
 * @property int $time Время
 * @property double $summa Сумма
 * @property int $count Количество
 * @property int $status Статус
 *
 * @property Rents[] $rents
 */
class PromoCodes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'promo_codes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['count'], 'required'],
            [['type', 'time', 'count', 'status'], 'integer'],
            [['summa'], 'number'],
            [['code'], 'string', 'max' => 255],
            [['time'], 'required', 'when' => function() {
                   if($this->type == 1) return TRUE;
                   else return FALSE;
            }],
            [['summa'], 'required', 'when' => function() {
                   if($this->type == 2) return TRUE;
                   else return FALSE;
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Код',
            'type' => 'Тип промокода',
            'time' => 'Время (мин)',
            'summa' => 'Сумма',
            'count' => 'Количество',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRents()
    {
        return $this->hasMany(Rents::className(), ['promo_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {

            $length = 8;
            $chars ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

            do {
                $string ='';
                for( $i = 0; $i < $length; $i++) {
                    $string .= $chars[rand(0,strlen($chars)-1)];
                }
            } while ( PromoCodes::find()->where(['code' => $string])->one() != null );

            $this->code = $string;
        }

        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        $rents = Rents::find()->where(['promo_id' => $this->id])->all();
        foreach ($rents as $value) {
            $value->promo_id = null;
            $value->save();
        }
        return parent::beforeDelete();
    }

    public function getTypeList()
    {
        return [
            1 => 'По времени поездки',
            2 => 'По сумме',
        ];
    }

    public function getStatusList()
    {
        return [
            1 => 'Новый',
            2 => 'Выпушен',
            3 => 'Использован',
        ];
    }

    public function setValues()
    {
        for ($i=0; $i < $this->count; $i++) { 
            $model = new PromoCodes();
            $model->type = $this->type;
            $model->time = $this->time;
            $model->summa = $this->summa;
            $model->status = $this->status;
            $model->count = 1;
            $model->save();
        }
    }
}
