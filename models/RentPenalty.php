<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "rent_penalty".
 *
 * @property int $id
 * @property int $rent_id Аренда
 * @property int $penalty_id Штраф
 * @property double $cost Цена
 *
 * @property Penalty $penalty
 * @property Rents $rent
 */
class RentPenalty extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rent_penalty';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rent_id', 'penalty_id'], 'integer'],
            [['cost'], 'number'],
            [['rent_id', 'cost'], 'required'],
            [['penalty_id'], 'exist', 'skipOnError' => true, 'targetClass' => Penalty::className(), 'targetAttribute' => ['penalty_id' => 'id']],
            [['rent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rents::className(), 'targetAttribute' => ['rent_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rent_id' => 'Аренда',
            'penalty_id' => 'Штраф',
            'cost' => 'Цена',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenalty()
    {
        return $this->hasOne(Penalty::className(), ['id' => 'penalty_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRent()
    {
        return $this->hasOne(Rents::className(), ['id' => 'rent_id']);
    }

    public function getPenaltyList()
    {
        $penalty = Penalty::find()->all();
        $result = [];
        foreach ($penalty as $value) {
            $result [] = [
                'id' => $value->id,
                'title' => 'Категория = ' . $value->getCategoriesList()[$value->category] . ( $value->damage !== null ? '; Тип повреждений = '. $value->getDamageList()[$value->damage] : '' ),
            ];
        }

        return ArrayHelper::map($result, 'id', 'title');
    }
}
