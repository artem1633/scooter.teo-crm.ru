<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $fio ФИО
 * @property string $telephone Телефон
 * @property string $permission Должность
 * @property string $login Логин
 * @property string $password Пароль
 */
class Users extends \yii\db\ActiveRecord
{
    const USER_ROLE_ADMIN = 'administrator';
    const USER_ROLE_MANAGER = 'manager';

    public $new_password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'permission', 'login', 'password', 'new_password'], 'string', 'max' => 255],
            [['login'], 'unique'],
            [['fio', 'password', 'login'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'permission' => 'Должность',
            'login' => 'Логин',
            'password' => 'Пароль',
            'new_password' => 'Новый пароль',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord){
            $this->password = Yii::$app->security->generatePasswordHash($this->password);
        }

        if($this->new_password != null) {
            $this->password = Yii::$app->security->generatePasswordHash($this->new_password);
        }
        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        $stations = Stations::find()->where(['user_id' => $this->id])->all();
        foreach ($stations as $value) {
            $value->delete();
        }

        return parent::beforeDelete();
    }

    public function getRoleList()
    {
        return ArrayHelper::map([
            ['id' => self::USER_ROLE_ADMIN, 'name' => 'Администратор',],
            ['id' => self::USER_ROLE_MANAGER, 'name' => 'Менеджер',],
        ], 'id', 'name');
    }

    public function getRoleDescription()
    {        
        if(self::USER_ROLE_ADMIN == $this->permission) return 'Администратор';
        if(self::USER_ROLE_MANAGER == $this->permission) return 'Менеджер';       
    }

    /*public function getAtelierList()
    {
        $ateliers = Atelier::find()->all();
        return ArrayHelper::map($ateliers, 'id', 'name');
    }*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStations()
    {
        return $this->hasMany(Stations::className(), ['user_id' => 'id']);
    }
}
