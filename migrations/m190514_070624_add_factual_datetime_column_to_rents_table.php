<?php

use yii\db\Migration;

/**
 * Handles adding factual_datetime to table `{{%rents}}`.
 */
class m190514_070624_add_factual_datetime_column_to_rents_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%rents}}', 'factual_datetime', $this->datetime()->comment('Фактическое время возврата'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%rents}}', 'factual_datetime');
    }
}
