<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%weekly_cost}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%tariffs}}`
 */
class m190526_053344_create_weekly_cost_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%weekly_cost}}', [
            'id' => $this->primaryKey(),
            'week_day' => $this->integer()->comment('День недели'),
            'begin_time' => $this->time()->comment('Время начала'),
            'end_time' => $this->time()->comment('Время окончание'),
            'cost' => $this->float()->comment('Стоимость'),
            'tariff_id' => $this->integer()->comment('Тариф'),
        ]);

        // creates index for column `tariff_id`
        $this->createIndex(
            '{{%idx-weekly_cost-tariff_id}}',
            '{{%weekly_cost}}',
            'tariff_id'
        );

        // add foreign key for table `{{%tariffs}}`
        $this->addForeignKey(
            '{{%fk-weekly_cost-tariff_id}}',
            '{{%weekly_cost}}',
            'tariff_id',
            '{{%tariffs}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%tariffs}}`
        $this->dropForeignKey(
            '{{%fk-weekly_cost-tariff_id}}',
            '{{%weekly_cost}}'
        );

        // drops index for column `tariff_id`
        $this->dropIndex(
            '{{%idx-weekly_cost-tariff_id}}',
            '{{%weekly_cost}}'
        );

        $this->dropTable('{{%weekly_cost}}');
    }
}
