<?php

use yii\db\Migration;

/**
 * Handles adding promo_id to table `{{%rents}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%promo_codes}}`
 */
class m190514_093726_add_promo_id_column_to_rents_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%rents}}', 'promo_id', $this->integer()->comment('Промо-код'));
        $this->addColumn('{{%rents}}', 'promo_code', $this->string(255)->comment('Промо-код'));

        // creates index for column `promo_id`
        $this->createIndex(
            '{{%idx-rents-promo_id}}',
            '{{%rents}}',
            'promo_id'
        );

        // add foreign key for table `{{%promo_codes}}`
        $this->addForeignKey(
            '{{%fk-rents-promo_id}}',
            '{{%rents}}',
            'promo_id',
            '{{%promo_codes}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%promo_codes}}`
        $this->dropForeignKey(
            '{{%fk-rents-promo_id}}',
            '{{%rents}}'
        );

        // drops index for column `promo_id`
        $this->dropIndex(
            '{{%idx-rents-promo_id}}',
            '{{%rents}}'
        );

        $this->dropColumn('{{%rents}}', 'promo_id');
        $this->dropColumn('{{%rents}}', 'promo_code');
    }
}
