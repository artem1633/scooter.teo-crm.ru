<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%settings}}`.
 */
class m190505_070717_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
            'key' => $this->string(255)->comment('Ключ'),
            'text' => $this->binary()->comment('Текст'),
        ]);

        $this->insert('settings',array(
            'name' => 'Пользовательская соглашения',
            'key' => 'policy',
            'text' => '<p style="text-align:center"><strong>ПОЛИТИКА ОБРАБОТКИ ПЕРСОНАЛЬНЫХ ДАННЫХ</strong></p>
                <p style="text-align:justify"><strong>Внимание!</strong><span style="color:rgb(0, 0, 0); font-family:times new roman; font-size:10pt"> Настоящий документ устанавливает порядок и условия обработки </span><span style="background-color:transparent; color:rgb(0, 0, 0); font-family:times new roman; font-size:10pt">Индивидуальным предпринимателем Ковальским Артемом Александровичем</span><em> </em><span style="color:rgb(0, 0, 0); font-family:times new roman; font-size:10pt">персональной информации, предоставляемой физическими лицами в процессе взаимодействия с оператором с применением сети Интернет. </span></p>',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%settings}}');
    }
}
