<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%penalty}}`.
 */
class m190505_135210_create_penalty_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%penalty}}', [
            'id' => $this->primaryKey(),
            'category' => $this->integer()->comment('Категория'),
            'price' => $this->float()->comment('Стоимость'),
            'damage' => $this->integer()->comment('Повреждения'),
        ]);

        $this->insert('{{%penalty}}',array(
            'category' => 1,
            'price' => 0,
            'damage' => null,
        ));

        $this->insert('{{%penalty}}',array(
            'category' => 2,
            'price' => 0,
            'damage' => null,
        ));

        $this->insert('{{%penalty}}',array(
            'category' => 3,
            'price' => 0,
            'damage' => 1,
        ));

        $this->insert('{{%penalty}}',array(
            'category' => 3,
            'price' => 0,
            'damage' => 2,
        ));

        $this->insert('{{%penalty}}',array(
            'category' => 3,
            'price' => 0,
            'damage' => 3,
        ));

        $this->insert('{{%penalty}}',array(
            'category' => 3,
            'price' => 0,
            'damage' => 4,
        ));

        $this->insert('{{%penalty}}',array(
            'category' => 3,
            'price' => 0,
            'damage' => 5,
        ));

        $this->insert('{{%penalty}}',array(
            'category' => 3,
            'price' => 0,
            'damage' => 6,
        ));

        $this->insert('{{%penalty}}',array(
            'category' => 3,
            'price' => 0,
            'damage' => 7,
        ));

        $this->insert('{{%penalty}}',array(
            'category' => 3,
            'price' => 0,
            'damage' => 8,
        ));

        $this->insert('{{%penalty}}',array(
            'category' => 3,
            'price' => 0,
            'damage' => 9,
        ));

        $this->insert('{{%penalty}}',array(
            'category' => 3,
            'price' => 0,
            'damage' => 10,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%penalty}}');
    }
}
