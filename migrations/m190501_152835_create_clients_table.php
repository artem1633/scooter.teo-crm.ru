<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%clients}}`.
 */
class m190501_152835_create_clients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%clients}}', [
            'id' => $this->primaryKey(),
            'phone' => $this->string(255)->comment('Телефон'),
            'surname' => $this->string(255)->comment('Фамилия'),
            'name' => $this->string(255)->comment('Имя'),
            'middle_name' => $this->string(255)->comment('Отчество'),
            'birthday' => $this->date()->comment('Дата рождения'),
            'payed_sum' => $this->float()->defaultValue(0)->comment('Оплаченная сумма всего'),
            'rental_hour' => $this->float()->defaultValue(0)->comment('Кол-во часов аренды'),
            'number_of_trips' => $this->float()->defaultValue(0)->comment('Кол-во поездок'),
            'passport_number' => $this->string(255)->comment('Серия и номер'),
            'passport_issue' => $this->text()->comment('Кем выдан паспорт'),
            'unit_code' => $this->string(255)->comment('Код подразделения'),
            'passport_photo' => $this->string(255)->comment('Фотография паспорта'),
            'agree' => $this->boolean()->defaultValue(0)->comment('Я согласен'),
            'status' => $this->integer()->comment('Статус'),
            'ip' => $this->string(255)->comment('IP'),
            'model' => $this->string(255)->comment('Модель устройства'),
            'datetime_agree' => $this->datetime()->comment('Дата и время'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%clients}}');
    }
}
