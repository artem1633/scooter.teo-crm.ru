<?php

use yii\db\Migration;

/**
 * Class m190507_103859_add_default_value_to_settings_table
 */
class m190507_103859_add_default_value_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings',array(
            'name' => 'Логин SMS Aero',
            'key' => 'login_sms_aero',
            'text' => 'sergey@berisamocat.ru',
        ));

        $this->insert('settings',array(
            'name' => 'Токен SMS Aero',
            'key' => 'token_sms_aero',
            'text' => 'b0v3fARjfj8vnT4myRMhpoTkzEH',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
