<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%rent_penalty}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%rents}}`
 * - `{{%penalty}}`
 */
class m190508_055955_create_rent_penalty_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%rent_penalty}}', [
            'id' => $this->primaryKey(),
            'rent_id' => $this->integer()->comment('Аренда'),
            'penalty_id' => $this->integer()->comment('Штраф'),
            'cost' => $this->float()->comment('Цена'),
        ]);

        // creates index for column `rent_id`
        $this->createIndex(
            '{{%idx-rent_penalty-rent_id}}',
            '{{%rent_penalty}}',
            'rent_id'
        );

        // add foreign key for table `{{%rents}}`
        $this->addForeignKey(
            '{{%fk-rent_penalty-rent_id}}',
            '{{%rent_penalty}}',
            'rent_id',
            '{{%rents}}',
            'id',
            'CASCADE'
        );

        // creates index for column `penalty_id`
        $this->createIndex(
            '{{%idx-rent_penalty-penalty_id}}',
            '{{%rent_penalty}}',
            'penalty_id'
        );

        // add foreign key for table `{{%penalty}}`
        $this->addForeignKey(
            '{{%fk-rent_penalty-penalty_id}}',
            '{{%rent_penalty}}',
            'penalty_id',
            '{{%penalty}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%rents}}`
        $this->dropForeignKey(
            '{{%fk-rent_penalty-rent_id}}',
            '{{%rent_penalty}}'
        );

        // drops index for column `rent_id`
        $this->dropIndex(
            '{{%idx-rent_penalty-rent_id}}',
            '{{%rent_penalty}}'
        );

        // drops foreign key for table `{{%penalty}}`
        $this->dropForeignKey(
            '{{%fk-rent_penalty-penalty_id}}',
            '{{%rent_penalty}}'
        );

        // drops index for column `penalty_id`
        $this->dropIndex(
            '{{%idx-rent_penalty-penalty_id}}',
            '{{%rent_penalty}}'
        );

        $this->dropTable('{{%rent_penalty}}');
    }
}
