<?php

use yii\db\Migration;

/**
 * Class m190511_154110_add_default_value_to_settings_table
 */
class m190511_154110_add_default_value_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings',array(
            'name' => 'Смс для клиента',
            'key' => 'client_sms',
            'text' => "Уважаемый(Уважаемая) {client_fio}. От нашего сайта {site_name} создан ссылка для подтверждения сведения про вас. Через этой ссылкой {link} войдите на сайт, подтвердите свою личность и принимайте пользовательского соглашения.",
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
