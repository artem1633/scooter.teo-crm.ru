<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%stations}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%users}}`
 */
class m190502_125352_create_stations_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%stations}}', [
            'id' => $this->primaryKey(),
            'location' => $this->text()->comment('Местоположение'),
            'user_id' => $this->integer()->comment('Менеджер'),
            'coordinate_x' => $this->string(255)->comment('Координата x'),
            'coordinate_y' => $this->string(255)->comment('Координата y'),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-stations-user_id}}',
            '{{%stations}}',
            'user_id'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-stations-user_id}}',
            '{{%stations}}',
            'user_id',
            '{{%users}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-stations-user_id}}',
            '{{%stations}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-stations-user_id}}',
            '{{%stations}}'
        );

        $this->dropTable('{{%stations}}');
    }
}
