<?php

use yii\db\Migration;

/**
 * Handles adding penalty_sum to table `{{%rents}}`.
 */
class m190508_154536_add_penalty_sum_column_to_rents_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%rents}}', 'penalty_sum', $this->float()->comment('Сумма штрафа'));
        $this->addColumn('{{%rents}}', 'status', $this->integer()->comment('Статус'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%rents}}', 'penalty_sum');
        $this->dropColumn('{{%rents}}', 'status');
    }
}
