<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%scooters}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%clients}}`
 */
class m190503_050825_create_scooters_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%scooters}}', [
            'id' => $this->primaryKey(),
            'qr_code' => $this->string(255)->comment('QR-код'),
            'status' => $this->integer()->comment('Статус'),
            'run' => $this->float()->comment('Пробег в км'),
            'state' => $this->integer()->comment('Состояние'),
            'client_id' => $this->integer()->comment('Клиент'),
            'coordinate_x' => $this->string(255)->comment('Координата x'),
            'coordinate_y' => $this->string(255)->comment('Координата y'),
        ]);

        // creates index for column `client_id`
        $this->createIndex(
            '{{%idx-scooters-client_id}}',
            '{{%scooters}}',
            'client_id'
        );

        // add foreign key for table `{{%clients}}`
        $this->addForeignKey(
            '{{%fk-scooters-client_id}}',
            '{{%scooters}}',
            'client_id',
            '{{%clients}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%clients}}`
        $this->dropForeignKey(
            '{{%fk-scooters-client_id}}',
            '{{%scooters}}'
        );

        // drops index for column `client_id`
        $this->dropIndex(
            '{{%idx-scooters-client_id}}',
            '{{%scooters}}'
        );

        $this->dropTable('{{%scooters}}');
    }
}
