<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tariffs}}`.
 */
class m190505_115008_create_tariffs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tariffs}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование тарифа'),
            'cost' => $this->float()->comment('Стоимость'),
            'type' => $this->integer()->comment('Тип'),
        ]);

        $this->insert('{{%tariffs}}',array(
            'name' => 'Поминутный',
            'cost' => 6,
            'type' => 1,
        ));

        $this->insert('{{%tariffs}}',array(
            'name' => 'Почасовой',
            'cost' => 300,
            'type' => 2,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%tariffs}}');
    }
}
