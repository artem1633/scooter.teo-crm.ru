<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%promo_codes}}`.
 */
class m190502_132848_create_promo_codes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%promo_codes}}', [
            'id' => $this->primaryKey(),
            'code' => $this->string(255)->comment('Код'),
            'type' => $this->integer()->comment('Тип промокода'),
            'time' => $this->integer()->comment('Время'),
            'summa' => $this->float()->comment('Сумма'),
            'count' => $this->integer()->comment('Количество'),
            'status' => $this->integer()->comment('Статус'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%promo_codes}}');
    }
}
