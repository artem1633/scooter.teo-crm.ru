<?php

use yii\db\Migration;

/**
 * Handles adding sms_text to table `{{%clients}}`.
 */
class m190507_111851_add_sms_text_column_to_clients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%clients}}', 'sms_text', $this->text()->comment('Текст смса'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%clients}}', 'sms_text');
    }
}
