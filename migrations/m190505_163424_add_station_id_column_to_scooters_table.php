<?php

use yii\db\Migration;

/**
 * Handles adding station_id to table `{{%scooters}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%stations}}`
 */
class m190505_163424_add_station_id_column_to_scooters_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%scooters}}', 'station_id', $this->integer()->comment('Станция'));

        // creates index for column `station_id`
        $this->createIndex(
            '{{%idx-scooters-station_id}}',
            '{{%scooters}}',
            'station_id'
        );

        // add foreign key for table `{{%stations}}`
        $this->addForeignKey(
            '{{%fk-scooters-station_id}}',
            '{{%scooters}}',
            'station_id',
            '{{%stations}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%stations}}`
        $this->dropForeignKey(
            '{{%fk-scooters-station_id}}',
            '{{%scooters}}'
        );

        // drops index for column `station_id`
        $this->dropIndex(
            '{{%idx-scooters-station_id}}',
            '{{%scooters}}'
        );

        $this->dropColumn('{{%scooters}}', 'station_id');
    }
}
