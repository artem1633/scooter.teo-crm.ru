<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%rents}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%stations}}`
 * - `{{%stations}}`
 * - `{{%scooter}}`
 * - `{{%clients}}`
 * - `{{%tariffs}}`
 */
class m190506_084855_create_rents_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%rents}}', [
            'id' => $this->primaryKey(),
            'begin_station' => $this->integer()->comment('Станция начала проката'),
            'end_station' => $this->integer()->comment('Станция окончания проката'),
            'scooter_id' => $this->integer()->comment('Самоката'),
            'begin_date' => $this->datetime()->comment('Время начала поездки'),
            'end_date' => $this->datetime()->comment('Время окончания поездки'),
            'client_id' => $this->integer()->comment('Клиента'),
            'tariff_id' => $this->integer()->comment('Тарифа'),
            'summa' => $this->float()->comment('Сумма'),
            'trip_duration' => $this->float()->comment('Длительности поездки'),
            'payment_status' => $this->boolean()->comment('Факт оплаты'),
            'payment_time' => $this->datetime()->comment('Дата и время оплаты'),
            'comment' => $this->text()->comment('Комментария'),
            'run_length' => $this->float()->comment('Пробег'),
        ]);

        // creates index for column `begin_station`
        $this->createIndex(
            '{{%idx-rents-begin_station}}',
            '{{%rents}}',
            'begin_station'
        );

        // add foreign key for table `{{%stations}}`
        $this->addForeignKey(
            '{{%fk-rents-begin_station}}',
            '{{%rents}}',
            'begin_station',
            '{{%stations}}',
            'id',
            'CASCADE'
        );

        // creates index for column `end_station`
        $this->createIndex(
            '{{%idx-rents-end_station}}',
            '{{%rents}}',
            'end_station'
        );

        // add foreign key for table `{{%stations}}`
        $this->addForeignKey(
            '{{%fk-rents-end_station}}',
            '{{%rents}}',
            'end_station',
            '{{%stations}}',
            'id',
            'CASCADE'
        );

        // creates index for column `scooter_id`
        $this->createIndex(
            '{{%idx-rents-scooter_id}}',
            '{{%rents}}',
            'scooter_id'
        );

        // add foreign key for table `{{%scooter}}`
        $this->addForeignKey(
            '{{%fk-rents-scooter_id}}',
            '{{%rents}}',
            'scooter_id',
            '{{%scooters}}',
            'id',
            'CASCADE'
        );

        // creates index for column `client_id`
        $this->createIndex(
            '{{%idx-rents-client_id}}',
            '{{%rents}}',
            'client_id'
        );

        // add foreign key for table `{{%clients}}`
        $this->addForeignKey(
            '{{%fk-rents-client_id}}',
            '{{%rents}}',
            'client_id',
            '{{%clients}}',
            'id',
            'CASCADE'
        );

        // creates index for column `tariff_id`
        $this->createIndex(
            '{{%idx-rents-tariff_id}}',
            '{{%rents}}',
            'tariff_id'
        );

        // add foreign key for table `{{%tariffs}}`
        $this->addForeignKey(
            '{{%fk-rents-tariff_id}}',
            '{{%rents}}',
            'tariff_id',
            '{{%tariffs}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%stations}}`
        $this->dropForeignKey(
            '{{%fk-rents-begin_station}}',
            '{{%rents}}'
        );

        // drops index for column `begin_station`
        $this->dropIndex(
            '{{%idx-rents-begin_station}}',
            '{{%rents}}'
        );

        // drops foreign key for table `{{%stations}}`
        $this->dropForeignKey(
            '{{%fk-rents-end_station}}',
            '{{%rents}}'
        );

        // drops index for column `end_station`
        $this->dropIndex(
            '{{%idx-rents-end_station}}',
            '{{%rents}}'
        );

        // drops foreign key for table `{{%scooter}}`
        $this->dropForeignKey(
            '{{%fk-rents-scooter_id}}',
            '{{%rents}}'
        );

        // drops index for column `scooter_id`
        $this->dropIndex(
            '{{%idx-rents-scooter_id}}',
            '{{%rents}}'
        );

        // drops foreign key for table `{{%clients}}`
        $this->dropForeignKey(
            '{{%fk-rents-client_id}}',
            '{{%rents}}'
        );

        // drops index for column `client_id`
        $this->dropIndex(
            '{{%idx-rents-client_id}}',
            '{{%rents}}'
        );

        // drops foreign key for table `{{%tariffs}}`
        $this->dropForeignKey(
            '{{%fk-rents-tariff_id}}',
            '{{%rents}}'
        );

        // drops index for column `tariff_id`
        $this->dropIndex(
            '{{%idx-rents-tariff_id}}',
            '{{%rents}}'
        );

        $this->dropTable('{{%rents}}');
    }
}
