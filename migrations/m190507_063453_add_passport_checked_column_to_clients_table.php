<?php

use yii\db\Migration;

/**
 * Handles adding passport_checked to table `{{%clients}}`.
 */
class m190507_063453_add_passport_checked_column_to_clients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%clients}}', 'passport_checked', $this->boolean()->comment('Паспорт проверен'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%clients}}', 'passport_checked');
    }
}
