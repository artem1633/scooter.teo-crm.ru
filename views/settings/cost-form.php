<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form padding_in_form">

    <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'week_day')->dropDownList($model->getWeekDay(),[]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'cost')->textInput(['type' => 'number']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, "begin_time", [])->widget(
                    \yii\widgets\MaskedInput::class, [
                        'mask' => "h:s",
                        'clientOptions' => [
                            'alias' => 'datetime',
                            "placeholder" => "чч:мм",
                            "separator" => "."
                        ],
                        'options' => []
                        ]
                    );
                ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, "end_time", [])->widget(
                    \yii\widgets\MaskedInput::class, [
                        'mask' => "h:s",
                        'clientOptions' => [
                            'alias' => 'datetime',
                            "placeholder" => "чч:мм",
                            "separator" => "."
                        ],
                        'options' => [ ]
                        ]
                    );
                ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>    
</div>
