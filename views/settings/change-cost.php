<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SettingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Настройка тарифа';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="settings-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_cost_columns.php'),
            'toolbar'=> [
                ['content'=>
                    Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['add-cost', 'id' => $id],
                    ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-primary']) .
                    Html::a('Назадь <i class="fa fa-reply-all"></i>', ['/settings/tariffs'],
                    ['data-pjax'=>'0','title'=> 'Назадь','class'=>'btn btn-default']).
                    '{toggleData}'
                ],
            ],          
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'responsiveWrap' => false,      
            'panel' => [
                'type' => 'danger', 
                'heading' => '<i class="glyphicon glyphicon-list"></i> Настройка',
                'after'=>'',
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
