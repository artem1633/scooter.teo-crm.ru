<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'category',
        'content' => function($data){
            return $data->getCategoriesList()[$data->category];
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'damage',
        'content' => function($data){
            return $data->getDamageList()[$data->damage];
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'price',
        'width' => '200px;',
        'content' => function($data){
            return Html::textInput('price', $data->price, [
                'class' =>'form-control',
                'type' => 'number',
                'onchange'=>"$.get('/settings/edit-price', {'id':$data->id, 'value':$(this).val()}, function(price){});",
            ]);
        },
    ],

];   