<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    /*[
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],*/
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'week_day',
        'content' => function($data){
            return $data->getWeekDay()[$data->week_day];
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'begin_time',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'end_time',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'cost',
        'content' => function($data){
            return $data->getCostDescription();
        }
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{update_tariff} {delete_tariff}',
        'buttons'  => [ 
            'update_tariff' => function ($url, $model) {
                $url = Url::to(['/settings/update-cost', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                    'role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'
                ]);
            },
            'delete_tariff' => function ($url, $model) {
                $url = Url::to(['/settings/delete-cost', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                    'role'=>'modal-remote','title'=>'Удалить', 
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Подтвердите действие',
                    'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                ]);
            },
        ],
    ],
];   