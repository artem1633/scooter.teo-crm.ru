<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    /*[
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],*/
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'cost',
    ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'text',
    ],*/
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{update_tariff} {change_cost} {delete_tariff}',
        'buttons'  => [ 
            'update_tariff' => function ($url, $model) {
                $url = Url::to(['/settings/update-tariff', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                    'role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'
                ]);
            },
            'change_cost' => function ($url, $model) {
                $url = Url::to(['/settings/change-cost', 'id' => $model->id]);
                return Html::a('<span class="fa fa-money"></span>', $url, [
                    'data-pjax'=>'0','title'=>'Настройка тарифа', 'data-toggle'=>'tooltip'
                ]);
            },
            'delete_tariff' => function ($url, $model) {
                if($model->id !== 1 && $model->id !== 2){
                    $url = Url::to(['/settings/delete-tariff', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'role'=>'modal-remote','title'=>'Удалить', 
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                    ]);                    
                }
            },
        ],
    ],

];   