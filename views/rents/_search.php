<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="client-form">
    <div class="box-body">
        <?php $form = ActiveForm::begin(['id' => 'search-form', 'options' => ['method' => 'post',]]); ?>
            <div class="row">
                <div class="col-md-4" >
                <label>Телефон номер клиента</label> 
                    <?=\yii\widgets\MaskedInput::widget([
                        'name' => 'phone',
                        'value' => $post['phone'],
                        'mask' => '+7(999)999-99-99',
                        'options' => ['class'=>'form-control',]
                    ])?>
                </div>  
                <div class="col-md-6">
                </div>
                <div class="form-group pull-right" >
                    <?= Html::submitButton('Найти клиента', ['class' => 'btn btn-info', 'style' => 'margin-top:25px;']) ?>        
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
