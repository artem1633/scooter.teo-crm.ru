<?php

use yii\helpers\Html;
?>

<div class="table-responsive">
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<!-- <th class="kv-align-center kv-align-middle" style="width: 3.35%;" data-col-seq="0">ID</th> -->
				<th data-col-seq="1" style="width: 15.95%;">Клиент</th>
				<th data-col-seq="2" style="width: 15.34%;">Самокат</th>
				<th data-col-seq="3" style="width: 15.06%;">Станция начала проката</th>
				<th data-col-seq="4" style="width: 15.41%;">Время начала поездки</th>
				<th data-col-seq="5" style="width: 15.21%;">Тариф</th>
				<th data-col-seq="6" style="width: 15.21%;">Статус аренды</th>
				<?php if($rent->status == 1){?><th data-col-seq="7" style="width: 15.21%;">Статус оплаты</th><?php } ?>
				<th class="kv-align-center kv-align-middle skip-export" style="width: 5.41%;" data-col-seq="8">Просмотр</th>
			</tr>
		</thead>
		<tbody>
			<tr data-key="1">
				<!-- <td class="kv-align-center kv-align-middle" style="width:30px;" data-col-seq="0"><?=$rent->id?></td> -->
				<td data-col-seq="1"><?=$rent->client->surname . ' ' . $rent->client->name . ' ' . $rent->client->middle_name?></td>
				<td data-col-seq="2"><?='Id=' . $rent->scooter->id . '; Qr-code='. $rent->scooter->qr_code?></td>
				<td data-col-seq="3"><?=$rent->beginStation->location;?></td>
				<td data-col-seq="4"><?=date('H:i d.m.Y', strtotime($rent->begin_date) )?></td>
				<td data-col-seq="5"><?=$rent->tariff->name?></td>
				<td data-col-seq="6"><?=$rent->getStatusDescription()?></td>
				<?php if($rent->status == 1){?><td data-col-seq="7"><?=$rent->getPaymentStatusDescription()?></td><?php } ?>
				<td class="skip-export kv-align-center kv-align-middle" style="width:50px;" data-col-seq="8">
					<a href="/rents/view?id=<?=$rent->id?>" title="Просмотр" aria-label="Просмотр" role="modal-remote" data-toggle="tooltip">
						<span class="glyphicon glyphicon-eye-open"></span>
					</a>
				</td>
			</tr>
		</tbody>
	</table>
</div>