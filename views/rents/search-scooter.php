<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="padding_in_form">
    <?php $form = ActiveForm::begin(['id' => 'search-form', 'options' => ['method' => 'post',]]); ?>
        <div class="row">
            <div class="col-md-12">
                <label>ID самоката</label> 
                <input type="number" class="form-control" required="" name="scooter">
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>