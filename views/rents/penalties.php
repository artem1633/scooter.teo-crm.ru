<?php
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Penalty;
use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;

CrudAsset::register($this);

?>
<?=GridView::widget([
    'id'=>'penalty-table',
    'dataProvider' => $penaltyProvider,
    'pjax'=>true,
    'responsiveWrap' => false,
    'columns' => [
        [
            'class' => 'kartik\grid\SerialColumn',
            'width' => '30px',
        ],

        /*[
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'rent_id',
            'content' => function($data){
                return ;
            }
        ],*/
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'penalty_id',
            'filter' => ArrayHelper::map(Penalty::find()->all(),'id', 'category'),
            'content' => function($data){
                return $data->getPenaltyList()[$data->penalty_id];
            }
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'cost',
        ],
        [ 
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'template' => '{delete}',
            'vAlign'=>'middle',
            'header' => 'Удалить',
            'buttons'  => [
                'delete' => function ($url, $model) {
                    if($model->rent->status == 0){
                        $url = Url::to(['/rents/delete-penalty', 'id' => $model->id]);
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'role'=>'modal-remote','title'=>'Удалить', 
                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                            'data-request-method'=>'post',
                            'data-toggle'=>'tooltip',
                            'data-confirm-title'=>'Подтвердите действие',
                            'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                        ]);
                    }
                },
            ],
        ],
    ],
    'toolbar'=> [
        ['content'=>
            $rent->status == 0 ? Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['/rents/create-penalty', 'id' => $rent->id], ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-primary']) : ''
        ],
    ],          
    'striped' => true,
    'condensed' => true,
    'responsive' => true,          
    'panel' => [
        'type' => 'primary', 
        'heading' => '<i class="glyphicon glyphicon-list"></i> Список штрафов',
        'after'=>'',
    ]
])?>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>