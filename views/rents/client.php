<?php

use yii\helpers\Html;

if (!file_exists('uploads/clients/' . $model->passport_photo) || $model->passport_photo == '') {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/no-image.jpg';
} else {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/uploads/clients/' . $model->passport_photo;
}

?>

<table class="kv-grid-table table table-bordered table-striped table-condensed">
	<thead>
		<tr>
			<th class="kv-align-center kv-align-middle" style="width: 2.35%;" data-col-seq="0">ID</th>
			<th data-col-seq="1" style="width: 10.95%;">Фото паспорта</th>
			<th data-col-seq="2" style="width: 12.41%;">Фамилия</th>
			<th data-col-seq="3" style="width: 10.21%;">Имя</th>
			<th data-col-seq="4" style="width: 11.58%;">Отчество</th>
			<th data-col-seq="5" style="width: 12.06%;">Телефон</th>
			<th data-col-seq="6" style="width: 15.34%;">Дата рождения</th>
			<th data-col-seq="7" style="width: 12.65%;">Статус</th>
			<th data-col-seq="8" style="width: 15.65%;">Паспорт</th>
			<th class="kv-align-center kv-align-middle skip-export" style="width: 5.41%;" data-col-seq="8">Просмотр</th>
		</tr>
	</thead>
	<tbody>
		<tr data-key="1">
			<td class="kv-align-center kv-align-middle" style="width:30px;" data-col-seq="0">1</td>
			<td data-col-seq="1"><?=Html::img($path, [ 'style' => 'width:70px;height:50px;'])?></td>
			<td data-col-seq="2"><?=$model->surname?></td>
			<td data-col-seq="3"><?=$model->name?></td>
			<td data-col-seq="4"><?=$model->middle_name?></td>
			<td data-col-seq="5"><?=$model->phone?></td>
			<td data-col-seq="6"><?=$model->birthday?></td>
			<td data-col-seq="7"><?=$model->getStatusDescription()?></td>
			<td data-col-seq="8"><?=$model->getPassportDescription()?></td>
			<td class="skip-export kv-align-center kv-align-middle" style="width:50px;" data-col-seq="8">
				<a href="/clients/view-cart?id=<?=$model->id?>" title="Просмотр" aria-label="Просмотр" role="modal-remote" data-toggle="tooltip">
					<span class="glyphicon glyphicon-eye-open"></span>
				</a>
			</td>
		</tr>
	</tbody>
</table>