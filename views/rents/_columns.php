<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Rents;

return [
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'id',
        'width' => '20px',
        'headerOptions'=>['class'=>'text-center'],
        'contentOptions'=>['class'=>'text-center'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'client_id',
        'filter' => Rents::getClientsList(),
        'content' => function($data){
            return $data->client->surname . ' ' . $data->client->name . ' ' . $data->client->middle_name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'begin_date',
        'content' => function($data){
            if($data->begin_date != null) return date('H:i d.m.Y', strtotime($data->begin_date) );
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'end_date',
        'content' => function($data){
            if($data->end_date != null) return date('H:i d.m.Y', strtotime($data->end_date) );
        }
    ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'trip_duration',
    ],*/
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'begin_station',
        'filter' => Rents::getStationsList(),
        'content' => function($data){
            return $data->beginStation->location;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'scooter_id',
        'filter' => Rents::getScootersList(),
        'content' => function($data){
            return 'Id=' . $data->scooter->id . '; Qr-code='. $data->scooter->qr_code . ( $data->status == 0 ? '<a class="btn btn-primary btn-xs" data-pjax="0" title="Вернуть самокат" href="'. Url::toRoute(['/rents/scooter-return', 'search' => $data->scooter_id]).'"><i class="fa fa-reply-all"></i></a>' : '');
        }
    ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'end_station',
        'filter' => Rents::getStationsList(),
        'content' => function($data){
            return $data->endStation->location;
        }
    ],*/
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'summa',
        'content' => function($data){
            return $data->summaDescription();
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'filter' => Rents::getStatusList(),
        'content' => function($data){
            return $data->getStatusDescription();
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'payment_status',
        'width' => '150px;',
        'filter'=> [0 => 'Нет', 1 => 'Да'],
        'content' => function($data){
            if($data->payment_status == 1) return $data->getPaymentStatusDescription();
            else return Html::dropDownList(
                'data', 
                $data->payment_status, 
                $data->getPaymentStatusList(), 
                [
                    'class' =>'form-control',
                    'onchange'=>"$.get('/rents/edit-payment-status', {'id':$data->id, 'value':$(this).val()}, function(data){} ); ",
                ]
            );
        }
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{view}',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   