<?php

use yii\helpers\Url;
use yii\helpers\Html;
use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;
use kartik\grid\GridView;

$this->title = 'Сдача самоката в аренду';
CrudAsset::register($this);
?>

<div class="row">
	<div class="col-md-12">
        <div class="box box-warning box-solid <?=$collapsed?>">
            <div class="box-header with-border">
          		<h3 class="box-title">Поиск</h3>
	           	<div class="box-tools pull-right">
	               	<button type="button" onclick="$.post('/site/button-position');" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-<?=$button?>"></i></button>
              	</div>
	        </div>
	        <div class="box-body">
	          <?= $this->render('_search', ['post' => $post]); ?>
	        </div>
      	</div>
    </div>	
</div>
<?php
    if($client_status === 1) 
    {
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-success box-solid <?=$collapsed?>">
            <div class="box-header with-border">
                <h3 class="box-title">Результат</h3>
            </div>
            <div class="box-body">
                <h3><center><b>Клиент уже взял самокат<br></b></center></h3>
            </div>
        </div>
    </div>
</div>
<?php 
    }else{ ?>
<?php if($client !== null) { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success box-solid <?=$collapsed?>">
                <div class="box-header with-border">
                    <h3 class="box-title">Результат</h3>
                </div>
                <div class="box-body">
                    <?= $this->render('client', ['model' => $client]); ?>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box box-success box-solid <?=$collapsed?>">
                <div class="box-header with-border">
                    <h3 class="box-title">Создать аренду</h3>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', ['model' => $model]); ?>
                </div>
            </div>
        </div>
    </div>
<?php 
    } 
    if($client === null && isset($post['phone'])) {
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-success box-solid <?=$collapsed?>">
            <div class="box-header with-border">
                <h3 class="box-title">Результат</h3>
            </div>
            <div class="box-body">
                <h3><center><b>Клиент не найдено<br></b></center></h3>
            </div>
        </div>
    </div>
</div>
<?php
    }
}
?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>