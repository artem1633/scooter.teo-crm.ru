<?php

use yii\widgets\DetailView;
use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rents */
$this->title = 'Просмотр';
CrudAsset::register($this);
?>
<div class="rents-view">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Карточка аренды</h3>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            [
                                'attribute' => 'status',
                                'value' => $model->status == 1 ? 'Завершено' : 'Не завершено',
                            ],
                            [
                                'attribute' => 'begin_station',
                                'value' => $model->beginStation->location,
                            ],
                            [
                                'attribute' => 'end_station',
                                'value' => $model->endStation->location,
                            ],
                            [
                                'attribute' => 'scooter_id',
                                'value' => 'Id = ' . $model->scooter->id . '; QR-код = ' . $model->scooter->qr_code,
                            ],
                            [
                                'attribute' => 'begin_date',
                                'value' => date('H:i d.m.Y', strtotime($model->begin_date) ),
                            ],
                            [
                                'attribute' => 'end_date',
                                'value' => $model->end_date ? date('H:i d.m.Y', strtotime($model->end_date) ) : '',
                            ],
                            [
                                'attribute' => 'factual_datetime',
                                'value' => $model->factual_datetime ? date('H:i d.m.Y', strtotime($model->factual_datetime) ) : '',
                            ],
                            [
                                'attribute' => 'client_id',
                                'value' => $model->client->surname . ' ' . $model->client->name . ' ' . $model->client->middle_name,
                            ],
                            [
                                'attribute' => 'tariff_id',
                                'value' => $model->tariff->name,
                            ],
                            [
                                'attribute' => 'promo_code',
                                'value' => $model->promoDescription(),
                            ],
                            'trip_duration',
                            [
                                'attribute' => 'summa',
                                'value' => $model->summaDescription(),
                            ],
                            'penalty_sum',
                            [
                                'attribute' => 'payment_status',
                                'format'=>'raw', 
                                'value' => $model->payment_status == 1 ? $model->getPaymentStatusDescription() : Html::dropDownList(
                                            'model', 
                                            $model->payment_status, 
                                            $model->getPaymentStatusList(), 
                                            [
                                                'class' =>'form-control',
                                                'onchange'=>"$.get('/rents/edit-payment-status', {'id':$model->id, 'value':$(this).val()}, function(data){} ); ",
                                            ]
                                        ),
                            ],
                            [
                                'attribute' => 'payment_time',
                                'value' => $model->payment_time !== null ? date('H:i d.m.Y', strtotime($model->payment_time)): '',
                            ],
                            'run_length',
                            'comment:html',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>