<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="rents-form padding_in_form">
    <?php $form = ActiveForm::begin(['id' => 'penalty-form', 'options' => ['method' => 'post']]); ?>
        <div class="row">
            <div class="col-md-9">
                <?= $form->field($model, 'penalty_id')->dropDownList(
                    $model->getPenaltyList(),
                    [
                        //'prompt' => 'Выберите',
                        'onchange' => '
                            $.post( "/rents/penalty-cost?id="+$(this).val(), function( data ){
                                $( "#price" ).val( data);
                            });
                        ',
                    ]
                ) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'cost')->textInput([ 'type' => 'number', 'id' => 'price']) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>