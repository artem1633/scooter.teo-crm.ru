<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Rents */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rents-form padding_in_form">

    <?php $form = ActiveForm::begin(['id' => 'rent-form', 'options' => ['method' => 'post']]); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'end_station')->dropDownList(
                $model->getStationsList(),
                [
                    /*'prompt' => 'Выберите',*/
                ]
            ) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'summa')->textInput(['type' => 'number', 'value' => $model->summaDescription()]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'penalty_sum')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'run_length')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'end_date')->textInput(['disabled' =>true, 'id' => 'end_date']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'comment')->textarea(['rows' => 2]) ?>
        </div>
    </div>
  
	<div class="form-group">
	    <?= Html::submitButton('Завершить поездку', ['class' => 'btn btn-success']) ?>
	</div>

    <?php ActiveForm::end(); ?>
    
</div>
