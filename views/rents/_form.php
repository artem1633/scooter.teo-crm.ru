<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Rents */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rents-form padding_in_form">

    <?php $form = ActiveForm::begin(['id' => 'rent-form', 'options' => ['method' => 'post',  'autocomplete'=>"off"]]); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'begin_station')->dropDownList(
                $model->getStationsList(),
                [
                    'prompt' => 'Выберите',
                    'onchange' => '
                        $.post( "/rents/scooter-list?id="+$(this).val(), function( data ){
                            $( "#scooter_id" ).html( data);
                        });
                    ',
                ]
            ) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'scooter_id')->dropDownList($model->getScootersList($model->begin_station),[ 'id' => 'scooter_id', 'prompt' => 'Выберите']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'tariff_id')->dropDownList($model->getTariffList(),
            [ 
                'id' => 'tariff_id', 
                /*'prompt' => 'Выберите'*/
                'onchange' => '
                    $.post( "/rents/change-end-time?trip_duration=" + $("#trip_duration").val() + "&begin_date=" + $("#rents-begin_date").val() + "&tariff_id=" + $("#tariff_id").val(), function( data ){
                        //alert(data);
                        $( "#end_date" ).val( data);
                    });
                ',
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'promo_code')->textInput([]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'payment_status')->dropDownList($model->getPaymentStatusList(),
            [ 
                'prompt' => 'Выберите'
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'trip_duration')->textInput([
                'type' => 'number',
                'id' => 'trip_duration',
                'onchange' => '
                    $.post( "/rents/change-end-time?trip_duration=" + $("#trip_duration").val() + "&begin_date=" + $("#rents-begin_date").val() + "&tariff_id=" + $("#tariff_id").val(), function( data ){
                        //alert(data);
                        $( "#end_date" ).val( data);
                    });
                ',
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model,"begin_date", [])->widget(
                    \yii\widgets\MaskedInput::class, [
                        'mask' => "h:s 1.2.y",
                        'clientOptions' => [
                            'alias' => 'datetime',
                            "placeholder" => "чч:мм дд.мм.гггг",
                            "separator" => "."
                        ],
                        'options' => [
                            'onchange'=>'function(e) { 
                                $.post( "/rents/change-end-time?trip_duration=" + $("#trip_duration").val() + "&begin_date=" + $("#rents-begin_date").val() + "&tariff_id=" + $("#tariff_id").val(), function( data ){
                                    var h = $("#trip_duration").val()  + $("#rents-begin_date").val() + $("#tariff_id").val();
                                    //alert(h);
                                    $( "#end_date" ).val( data);
                                });
                            }'
                        ]
                    ]
                );
            ?>
            <?php /*$form->field($model, 'begin_date')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Выберите ...'],
                'pluginEvents' => [
                    "changeDate" => 'function(e) { 
                        $.post( "/rents/change-end-time?trip_duration=" + $("#trip_duration").val() + "&begin_date=" + $("#rents-begin_date").val() + "&tariff_id=" + $("#tariff_id").val(), function( data ){
                            var h = $("#trip_duration").val()  + $("#rents-begin_date").val() + $("#tariff_id").val();
                            //alert(h);
                            $( "#end_date" ).val( data);
                        });
                    }',
                ],
                'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => false,
                'pluginOptions' => [
                    'format' => 'hh:ii dd.mm.yyyy',
                    'autoclose' => true
                ]
            ]);*/ ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'end_date')->textInput(['disabled' =>true, 'id' => 'end_date']) ?>
        </div>
    </div>

    <?= $form->field($model, 'client_id')->hiddenInput()->label(false) ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton('Сдать самокат', ['class' => 'btn btn-success']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
