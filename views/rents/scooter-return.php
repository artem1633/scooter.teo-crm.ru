<?php

use yii\helpers\Url;
use yii\helpers\Html;
use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Возврат самоката';
CrudAsset::register($this);
?>

<?php Pjax::begin(['enablePushState' => false, 'id' => 'scooter-pjax']) ?>

<?php if($rent !== null) { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success box-solid <?=$collapsed?>">
                <div class="box-header with-border">
                    <h3 class="box-title">Информация</h3>
                </div>
                <div class="box-body">
                    <?= $this->render('rent', ['rent' => $rent]); ?>
                </div>
            </div>
        </div>
        <div class="col-md-12">
                <?= $this->render('penalties', ['penaltyProvider' => $penaltyProvider, 'rent' => $rent]); ?>
        </div>
        <?php if($rent->status == 0) {?>
            <div class="col-md-12">
                <div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Завершить аренду</h3>
                    </div>
                    <div class="box-body">
                        <?= $this->render('_form2', ['model' => $rent]); ?>
                    </div>
                </div>
            </div>
        <?php }?>
    </div>
<?php } else {?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-success box-solid <?=$collapsed?>">
            <div class="box-header with-border">
                <h3 class="box-title">Результат</h3>
            </div>
            <div class="box-body">
                <h3><center><b>Этот самокат не существует или не дано на аренду<br></b></center></h3>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php Pjax::end() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>