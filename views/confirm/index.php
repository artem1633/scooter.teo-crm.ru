<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\models\Clients;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
\johnitvn\ajaxcrud\CrudAsset::register($this);
$this->title = 'Соглашения';

if (!file_exists('uploads/clients/'.$model->passport_photo) || $model->passport_photo == '') {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/no-image.jpg';
} else {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/uploads/clients/'.$model->passport_photo;
}

?>
<?php Pjax::begin(['enablePushState' => false, 'id' => 'personal-pjax']) ?>
<div class="box box-solid box-info">
    <div class="box-header">
        <h3 class="box-title">
        	Информация 
        </h3>
    </div>
	<div class="row">
	    <div class="col-md-12">
	       	<div class="table-responsive">
	       	    <table class="table table-bordered">
	       	        <tbody>
	       	        <tr>
	       	            <td><b><?=$model->getAttributeLabel('passport_photo')?></b></td>
	       	            <td><b><?=$model->getAttributeLabel('surname')?> <?=$model->getAttributeLabel('name')?> <?=$model->getAttributeLabel('middle_name')?></b></td>
	                    <td><?=Html::encode($model->surname)?> <?=Html::encode($model->name)?> <?=Html::encode($model->middle_name)?></td>
	                </tr>
	                <tr>
	                    <td rowspan="7"><img style="height:200px;" src="<?=$path?>"></td>
	                </tr>
	                <tr>
	                    <td><b><?=$model->getAttributeLabel('phone')?></b></td>
	                    <td><?=Html::encode($model->phone)?></td>
	                </tr>
	                <tr>
	                    <td><b><?=$model->getAttributeLabel('birthday')?></b></td>
	                    <td><?=Html::encode($model->birthday !== null ? \Yii::$app->formatter->asDate($model->birthday, 'php:d.m.Y') : '')?></td>
	                </tr>
	                <tr>
	                    <td><b><?=$model->getAttributeLabel('passport_number')?></b></td>
	                    <td><?=Html::encode($model->passport_number)?></td>
	                </tr>
	                <tr>
	                    <td><b><?=$model->getAttributeLabel('unit_code')?></b></td>
	                    <td><?=Html::encode($model->unit_code)?></td>
	                </tr>
	                <tr>
	                    <td><b><?=$model->getAttributeLabel('passport_issue')?></b></td>
	                    <td><?=Html::encode($model->passport_issue)?></td>
	                </tr>
	                <tr>
	                    <td><b><?=$model->getAttributeLabel('status')?></b></td>
	                    <td><b class="label label-warning"><?=$model->getStatusDescription()?></b></td>
	                </tr>
	                <?php if($model->status == Clients::STATUS_NO_ACCESS){?>
	                <tr>
                        <td colspan="3">
                        	<?php $form = ActiveForm::begin(); ?>
                        		<?= $form->field($model, 'agree')->checkbox(['id'=>"agree"])->label(false) ?>
		                        <?= Html::a('Политика в отношении обработки персональных данных', ['/confirm/policy'],
		                            ['data-pjax' => 0, 'target' => '_blank']) ?>
                        	<?php ActiveForm::end(); ?>
                        </td>
                    </tr>
	                <tr id="button">
                        <td colspan="3">
					        <a class="btn btn-warning" style="width: 100%" role="modal-remote" href="<?=Url::toRoute(['check', 'id' => $model->id])?>">Подтвердить</a>
                        </td>
                    </tr>
				    <?php }?>
	                </tbody>
	            </table>
	        </div>
	    </div>
	</div>
</div>
<?php Pjax::end() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    "size" => "normal",
    "options" => [
        "tabindex" => false,
    ],
])?>
<?php Modal::end(); ?>

<?php 
$this->registerJs(<<<JS
   
	var checked = $('#agree').is(':checked');
    if(checked == 0) $('#button').hide();
    else $('#button').show();

    $('#agree').on('change', function() 
    {
        var checked = $('#agree').is(':checked');
        if(checked == 0) {
            $('#button').hide();
        }
        else {
            $('#button').show();
        }
    });

JS
);
?>