<?php
use yii\helpers\Url;
use app\models\Rents;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'rent_id',
        'label' => '№ аренды',
        'content' => function($data){
            return $data->rent->id;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'penalty_id',
        'content' => function($data){
            return $data->getPenaltyList()[$data->penalty_id];
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'cost',
    ],
];   