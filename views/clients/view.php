<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Clients;
use yii\widgets\Pjax;
use kartik\grid\GridView;

\johnitvn\ajaxcrud\CrudAsset::register($this);
$this->title = 'Клиенты';

if (!file_exists('uploads/clients/'.$model->passport_photo) || $model->passport_photo == '') {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/no-image.jpg';
} else {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/uploads/clients/'.$model->passport_photo;
}

?>
    
<div class="box box-solid box-warning">
    <div class="box-header">
        <h3 class="box-title">Просмотр/Изменение</h3>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs padding_in_form">
                <li class="active">
                    <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                        <span class="visible-xs">Общая информация</span>
                        <span class="hidden-xs">Общая информация</span>
                    </a>
                </li>
                <li class="">
                    <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs">История поездки</span>
                        <span class="hidden-xs">История поездки</span>
                    </a>
                </li>
                <li class="">
                    <a href="#default-tab-3" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs">Оплаты</span>
                        <span class="hidden-xs">Оплаты</span>
                    </a>
                </li>
                <li class="">
                    <a href="#default-tab-4" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs">Штрафы</span>
                        <span class="hidden-xs">Штрафы</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content padding_in_form">
                <div class="tab-pane fade active in" id="default-tab-1">
                    <div class="row">
                        <?php Pjax::begin(['enablePushState' => false, 'id' => 'personal-pjax']) ?>
                        <div class="col-md-12">
                            <h3>Персональная информация <a class="btn btn-primary" role="modal-remote" href="<?=Url::toRoute(['update', 'id' => $model->id])?>"><i class="fa fa-pencil"></i></a></h3>
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td width="200px;"><b><?=$model->getAttributeLabel('passport_photo')?></b></td>
                                        <td width="200px;"><b><?=$model->getAttributeLabel('surname')?> <?=$model->getAttributeLabel('name')?> <?=$model->getAttributeLabel('middle_name')?></b></td>
                                        <td width="250px;"><?=Html::encode($model->surname)?> <?=Html::encode($model->name)?> <?=Html::encode($model->middle_name)?></td>
                                        <?php if($model->status == Clients::STATUS_ACCESS){ ?>
                                            <td><b><?=$model->getAttributeLabel('datetime_agree')?></b></td>
                                            <td><?=Html::encode(date('H:i d.m.Y', strtotime($model->datetime_agree)))?></td>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <td rowspan="7"><img style="height:200px;" src="<?=$path?>"></td>
                                    </tr>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('phone')?></b></td>
                                        <td><a href="tel:<?=$model->phone?>"><?=$model->phone?></a></td>
                                        <?php if($model->status == Clients::STATUS_ACCESS){ ?>
                                            <td><b><?=$model->getAttributeLabel('ip')?></b></td>
                                            <td><?=Html::encode($model->ip)?></td>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('birthday')?></b></td>
                                        <td><?=$model->birthday?></td>
                                        <?php if($model->status == Clients::STATUS_ACCESS){ ?>
                                            <td colspan="2"><b><?=$model->getAttributeLabel('model')?></b></td>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('passport_number')?></b></td>
                                        <td><?=Html::encode($model->passport_number)?></td>
                                        <?php if($model->status == Clients::STATUS_ACCESS){ ?>
                                            <td colspan="2"><?=$model->model?></td>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('unit_code')?></b></td>
                                        <td><?=Html::encode($model->unit_code)?></td>
                                    </tr>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('passport_issue')?></b></td>
                                        <td><?=Html::encode($model->passport_issue)?></td>
                                    </tr>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('status')?></b></td>
                                        <td><b class="<?=$model->status == Clients::STATUS_ACCESS ? 'label label-success' : 'label label-warning'?>"><?=$model->getStatusDescription()?></b></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?php Pjax::end() ?>
                    </div>
                </div>
                <div class="tab-pane fade" id="default-tab-2">
                    <div class="row">
                        <div class="col-md-12">
                        <?=GridView::widget([
                            'id'=>'history-datatable',
                            'dataProvider' => $historyProvider,
                            'pjax'=>true,
                            'responsiveWrap' => false,
                            'columns' => require(__DIR__.'/history_rent_columns.php'),
                            'toolbar'=> [
                                ['content'=> ''],
                            ],
                            'striped' => true,
                            'condensed' => true,
                            'responsive' => true,
                            'panel' => [
                                'type' => 'primary', 
                                'heading' => '<i class="glyphicon glyphicon-list"></i> История',
                                'after'=>'',
                            ]
                        ])?>
                        </div>
                    </div>        
                </div>
                <div class="tab-pane fade" id="default-tab-3">
                    <div class="row">
                        <div class="col-md-12">
                            <?=GridView::widget([
                                'id'=>'history-datatable',
                                'dataProvider' => $payedProvider,
                                'pjax'=>true,
                                'responsiveWrap' => false,
                                'columns' => require(__DIR__.'/payed_rent_columns.php'),
                                'toolbar'=> [
                                    ['content'=> ''],
                                ],          
                                'striped' => true,
                                'condensed' => true,
                                'responsive' => true,
                                'panel' => [
                                    'type' => 'primary', 
                                    'heading' => '<i class="glyphicon glyphicon-list"></i> Оплаты',
                                    'after'=>'',
                                ]
                            ])?>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="default-tab-4">
                    <div class="row">
                        <div class="col-md-12">
                            <?=GridView::widget([
                                'id'=>'penalty-datatable',
                                'dataProvider' => $penaltyProvider,
                                'pjax'=>true,
                                'responsiveWrap' => false,
                                'columns' => require(__DIR__.'/penalty_columns.php'),
                                'toolbar'=> [
                                    ['content'=> ''],
                                ],          
                                'striped' => true,
                                'condensed' => true,
                                'responsive' => true,
                                'panel' => [
                                    'type' => 'primary', 
                                    'heading' => '<i class="glyphicon glyphicon-list"></i> Штрафы',
                                    'after'=>'',
                                ]
                            ])?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    "size" => "modal-lg",
    "options" => [
        "open.bs.modal" => "function(){ console.log('123'); }",
        "tabindex" => false,
    ],
])?>
<?php Modal::end(); ?>