<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-form padding_in_form">

    <?php $form = ActiveForm::begin([ 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), 
            ['mask' => '+7(999)999-99-99','options' => ['class'=>'form-control',]]) ?>
        </div>
        <div class="col-md-4">
            <?php /*$form->field($model, 'birthday')->widget(kartik\date\DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите'],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy',
                ]
            ]);*/
            ?>
            <?= $form->field($model,"birthday", [])->widget(
                    \yii\widgets\MaskedInput::class, [
                        'mask' => "1.2.y",
                        'clientOptions' => [
                            'alias' => 'datetime',
                            "placeholder" => "дд.мм.гггг",
                            "separator" => "."
                        ],
                    ]
                );
            ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'passport_number')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9999-999999',
                'options' => ['class'=>'form-control',]
                ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'passport_issue')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'unit_code')->widget(\yii\widgets\MaskedInput::className(), 
                ['mask' => '999-999','options' => ['class'=>'form-control',]]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div id="images-to-upload">
                <?= $model->passport_photo != null ? '<img style="width:100%;height:200px;" src="http://' . $_SERVER["SERVER_NAME"] . "/uploads/clients/" . $model->passport_photo .' ">' : '' ?>
            </div>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'file')->fileInput(['class'=>"btn_image"]); ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    
</div>

<?php 
$this->registerJs(<<<JS

$(document).ready(function(){
    var fileCollection = new Array();

    $(document).on('change', '.btn_image', function(e){
        var files = e.target.files;
        var button_id = $(this).attr("id");
        //alert(button_id);
        $.each(files, function(i, file){
            fileCollection.push(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){
                var template = '<img style="width:100%;height:200px;" src="'+e.target.result+'"> ';
                $('#images-to-upload').html('');
                //document.getElementById('theID').value = 'new value';
                $('#images-to-upload').append(template);
            };
        });
    });

});
JS
);
?>

