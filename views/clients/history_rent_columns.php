<?php
use yii\helpers\Url;
use app\models\Rents;

return [
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'id',
        'headerOptions'=>['class'=>'text-center'],
        'contentOptions'=>['class'=>'text-center'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'begin_date',
        'content' => function($data){
           if($data->begin_date != null) return date('H:i d.m.Y', strtotime($data->begin_date) );
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'end_date',
        'content' => function($data){
            if($data->end_date != null)return date('H:i d.m.Y', strtotime($data->end_date) );
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'trip_duration',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'scooter_id',
        'filter' => Rents::getScootersList(),
        'content' => function($data){
            return 'Id=' . $data->scooter->id . '; Qr-code='. $data->scooter->qr_code;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'begin_station',
        'filter' => Rents::getStationsList(),
        'content' => function($data){
            return $data->beginStation->location;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'end_station',
        'filter' => Rents::getStationsList(),
        'content' => function($data){
            return $data->endStation->location;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'summa',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'penalty_sum',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'payment_status',
        'filter'=> [0 => 'Нет', 1 => 'Да'],
        'content' => function($data){
            return $data->getPaymentStatusDescription();
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'filter'=> [0 => 'Не завершено', 1 => 'Завершено'],
        'content' => function($data){
            return $data->getStatusDescription();
        }
    ],
];   