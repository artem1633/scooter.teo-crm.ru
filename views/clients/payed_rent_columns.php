<?php
use yii\helpers\Url;
use app\models\Rents;

return [
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'id',
        'headerOptions'=>['class'=>'text-center'],
        'contentOptions'=>['class'=>'text-center'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'trip_duration',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'scooter_id',
        'filter' => Rents::getScootersList(),
        'content' => function($data){
            return 'Id=' . $data->scooter->id . '; Qr-code='. $data->scooter->qr_code;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'summa',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'penalty_sum',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'summa',
        'label' => 'Оплачено',
        'content' => function($data){
            return ($data->summa + $data->penalty_sum);
        }
    ],
];   