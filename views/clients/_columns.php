<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'passport_photo',
        'label' => 'Фото паспорта',
        'content' => function($data){
            if (!file_exists('uploads/clients/' . $data->passport_photo) || $data->passport_photo == '') {
                $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/no-image.jpg';
            } else {
                $path = 'http://' . $_SERVER['SERVER_NAME'].'/uploads/clients/' . $data->passport_photo;
            }
            return Html::img($path, [ 'style' => 'width:70px;height:50px;']);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'surname',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'middle_name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'phone',
        'filter' => \yii\widgets\MaskedInput::widget([
            'name' => 'ClientsSearch[phone]',
            'value' => $searchModel->phone,
            'mask' => ['+7(999)999-99-99']
        ]),
        'content' => function($data){
            return '<a href="tel:'.$data->phone.'">'.$data->phone.'</a>';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'birthday',
        /*'filter' => kartik\date\DatePicker::widget([
            'model' => $searchModel,
            'attribute' => 'birthday',
            'removeButton' => false,
            //'template' => '{addon}{input}',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',
                ]
        ]),*/
        /*'content' => function($data){
            if($data->birthday !== null) return date('d.m.Y', strtotime($data->birthday));
        }*/
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'filter' => [0 => 'Не подтверждён', 1 => 'Подтверждён'],
        'content' => function($data){
            return $data->getStatusDescription();
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'passport_checked',
        'filter' => [0 => 'Пасспорт не проверен', 1 => 'Пасспорт проверен'],
        'content' => function($data){
            return $data->getPassportDescription();
        }
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{view} {delete}',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['data-pjax'=>'0','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'], 
    ],

];   