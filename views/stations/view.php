<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Stations */
?>
<div class="stations-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'location:ntext',
            //'user_id',
            [
                'attribute' => 'user_id',
                'value' => $model->user->fio,
            ],
            'coordinate_x',
            'coordinate_y',
        ],
    ]) ?>

</div>
