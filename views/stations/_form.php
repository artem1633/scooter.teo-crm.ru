<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Stations */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stations-form padding_in_form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'location')->textarea(['rows' => 1]) ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'user_id')->dropDownList($model->getUsersList(),['prompt' => 'Выберите']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h3>GPS координаты</h3>
        </div>
        <hr>
        <div class="col-md-6">
            <?= $form->field($model, 'coordinate_x')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'coordinate_y')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
