<?php
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Scooters;

?>

<center><h4 class="text-danger"><b>Список самокатов на текущей станции</b></h4></center>

<?= GridView::widget([ 
    'id'=>'scooters-list-'.$model->id,
    'dataProvider' => $model->ScootersList,
    'columns' => [
        [
	        'class'=>'\kartik\grid\DataColumn',
	        'attribute'=>'id',
	    ],
        [
	        'class'=>'\kartik\grid\DataColumn',
	        'attribute'=>'qr_code',
	    ],
	    [
	        'class'=>'\kartik\grid\DataColumn',
	        'attribute'=>'station_id',
	        'filter' => Scooters::getStationList(),
	        'content' => function($data){
	            return $data->getStationList()[$data->station_id];
	        }
	    ],
	    [
	        'class'=>'\kartik\grid\DataColumn',
	        'attribute'=>'status',
	        'filter' => Scooters::getStationList(),
	        'filter' => Scooters::getStatusList(),
	        'content' => function($data){
	            return $data->getStatusDescription();
	        }
	    ],
	    [
	        'class'=>'\kartik\grid\DataColumn',
	        'attribute'=>'run',
	    ],
	    [
	        'class'=>'\kartik\grid\DataColumn',
	        'attribute'=>'state',
	        'filter' => Scooters::getStateList(),
	        'content' => function($data){
	            return $data->getStateDescription();
	        }
	    ],
	    [
	        'class'=>'\kartik\grid\DataColumn',
	        'attribute'=>'client_id',
	        'filter' => Scooters::getClientsList(),
	        'content' => function($data){
	            return $data->getClientsList()[$data->client_id];
	        }
	    ],
    ],
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
]) ?>
