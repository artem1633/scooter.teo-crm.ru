<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Stations */

?>
<div class="stations-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
