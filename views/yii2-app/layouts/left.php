<?php

use app\models\Users;

?>
<aside class="main-sidebar">
    <section class="sidebar">
    <?php 
        if(isset(Yii::$app->user->identity->id))
        { 
            $permission = Yii::$app->user->identity->permission;
            echo dmstr\widgets\Menu::widget([
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    //['label' => 'Menu', 'options' => ['class' => 'header']],
                    //['label' => 'Dashboard', 'icon' => 'desktop', 'url' => ['/indicators'],],
                    ['label' => 'Клиенты', 'icon' => 'users', 'url' => ['/clients'],],
                    ['label' => 'Самокаты', 'icon' => 'cube', 'url' => ['/scooters'],],
                    ['label' => 'Дашбоард', 'icon' => 'archive', $permission == 'administrator', 'url' => ['/rents/list'],],
                    ['label' => 'Сдача самоката в аренду', 'icon' => 'gg-circle', 'url' => ['/rents'],],
                    /*['label' => 'Вернуть самокат', 'icon' => 'cubes', 'role'=>'modal-remote', 'url' => ['/rents/scooter-return'],],*/
                    [
                        'label' => 'Справочники',
                        'icon' => 'wrench',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Пользователи', 'icon' => 'book', 'visible' => $permission == 'administrator', 'url' => ['/users'],],
                            ['label' => 'Станции', 'icon' => 'book', 'url' => ['/stations'],],
                            ['label' => 'Настройка', 'icon' => 'book', $permission == 'administrator', 'url' => ['/settings'],],
                            ['label' => 'Тарифы', 'icon' => 'book', 'visible' => $permission == 'administrator', 'url' => ['/settings/tariffs'],],
                            ['label' => 'Штрафы', 'icon' => 'book', 'visible' => $permission == 'administrator', 'url' => ['/settings/penalty'],],
                            ['label' => 'Промокоды', 'icon' => 'book', 'url' => ['/promo-codes'],],
                        ],
                    ],
                ],
            ]);
        } else {
            echo dmstr\widgets\Menu::widget([
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Соглашения', /*'icon' => 'desktop',*/ 'url' => ['/confirm'],],
                ],
            ]);
        }
    ?>
    <?php 
        if(isset(Yii::$app->user->identity->id)) { ?>
            <a href="/rents/search-scooter" role="modal-remote" > 
                &nbsp; &nbsp; <i class="fa fa-reply-all"></i>&nbsp;&nbsp;<span>Вернуть самокат</span>
            </a>
    <?php } ?>
    </section>
</aside>