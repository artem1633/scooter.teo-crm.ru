<?php

use app\models\Users;
$pathInfo = Yii::$app->request->pathInfo;
$class = '';
if($pathInfo == 'stations' || $pathInfo == 'promo-codes' || $pathInfo == 'users' || $pathInfo == 'settings' || $pathInfo == 'settings/tariffs' || $pathInfo == 'settings/penalty') $class = 'active menu-open';
?>
<aside class="main-sidebar">
	<section class="sidebar">
		<?php 
        	if(isset(Yii::$app->user->identity->id))
        	{ 
            	$permission = Yii::$app->user->identity->permission;
        ?>
	    <ul class="sidebar-menu tree" data-widget="tree">
	    	<li <?= ($pathInfo == 'clients' ? 'class="active"' : '')?> >
	    		<a href="/clients">
	    			<i class="fa fa-users"></i>  
	    			<span>Клиенты</span>
	    		</a>
	    	</li>
			<li <?= ($pathInfo == 'rents' ? 'class="active"' : '')?> ><a href="/rents"><i class="fa fa-gg-circle"></i>  <span>Сдать самокат</span></a></li>
			<li <?= ($pathInfo == 'rents/search-scooter' ? 'class="active"' : '')?> ><a href="/rents/search-scooter" role="modal-remote"><i class="fa fa-reply-all"></i>  <span>Вернуть самокат</span></a></li>
			<li <?= ($pathInfo == 'scooters' ? 'class="active"' : '')?> ><a href="/scooters"><i class="fa fa-cube"></i>  <span>Самокаты</span></a></li>
			<?php if($permission == 'administrator') { ?>
				<li <?= ($pathInfo == 'rents/list' ? 'class="active"' : '')?> ><a href="/rents/list"><i class="fa fa-archive"></i>  <span>Дашбоард</span></a></li>
			<?php } ?>
			<li class="treeview <?=$class?>"><a href="#"><i class="fa fa-wrench"></i>  <span>Справочники</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
				<ul class="treeview-menu">
					<li <?= ($pathInfo == 'stations' ? 'class="active"' : '')?> ><a href="/stations"><i class="fa fa-book"></i>  <span>Станции</span></a></li>
					<li <?= ($pathInfo == 'promo-codes' ? 'class="active"' : '')?> ><a href="/promo-codes"><i class="fa fa-book"></i>  <span>Промокоды</span></a></li>
					<?php if($permission == 'administrator') { ?>
						<li <?= ($pathInfo == 'users' ? 'class="active"' : '')?> ><a href="/users"><i class="fa fa-book"></i>  <span>Пользователи</span></a></li>
						<li <?= ($pathInfo == 'settings' ? 'class="active"' : '')?> ><a href="/settings"><i class="fa fa-book"></i>  <span>Настройка</span></a></li>
						<li <?= ($pathInfo == 'settings/tariffs' ? 'class="active"' : '')?> ><a href="/settings/tariffs"><i class="fa fa-book"></i>  <span>Тарифы</span></a></li>
						<li <?= ($pathInfo == 'settings/penalty' ? 'class="active"' : '')?> ><a href="/settings/penalty"><i class="fa fa-book"></i>  <span>Штрафы</span></a></li>
					<?php } ?>
				</ul>
			</li>
		</ul>
		<?php
			} else {
		?>
			<ul class="sidebar-menu tree" data-widget="tree">
		    	<li><a href="/confirm"><i class="fa fa-users"></i>  <span>Соглашения</span></a></li>
			</ul>
		<?php } ?>
	</section>
</aside>