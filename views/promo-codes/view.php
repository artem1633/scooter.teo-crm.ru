<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PromoCodes */
?>
<div class="promo-codes-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'code',
            'type',
            'time:datetime',
            'summa',
            'count',
            'status',
        ],
    ]) ?>

</div>
