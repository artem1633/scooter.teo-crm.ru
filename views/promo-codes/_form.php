<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PromoCodes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="promo-codes-form padding_in_form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'type')->dropDownList($model->getTypeList(),['id' => 'type']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'count')->textInput(['type' => 'number']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6" id="time" <?php if($model->type != 1) echo 'style=" display: none;"';?>>
            <?= $form->field($model, 'time')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-6" id="summa" <?php if($model->type != 2) echo 'style=" display: none;"';?>>
            <?= $form->field($model, 'summa')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'status')->dropDownList($model->getStatusList(),[]) ?>
        </div>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
<?php 
$this->registerJs(<<<JS
    $('#type').on('change', function() 
    {  
        var type = this.value; //console.log(type);
        $('#time').hide(); $('#summa').hide();
        if(type == 1) $('#time').show(); 
        if(type == 2) $('#summa').show(); 
    }
);
JS
);
?>