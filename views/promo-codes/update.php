<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PromoCodes */
?>
<div class="promo-codes-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
