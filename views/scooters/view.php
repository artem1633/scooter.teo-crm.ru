<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Scooters */
?>
<div class="scooters-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'qr_code',
            [
                'attribute' => 'station_id',
                'value' => $model->getStationList()[$model->station_id],
            ],
            [
                'attribute' => 'status',
                'value' => $model->getStatusDescription(),
            ],
            'run',
            [
                'attribute' => 'state',
                'value' => $model->getStateDescription(),
            ],
            [
                'attribute' => 'client_id',
                'value' => $model->getClientsList()[$model->client_id],
            ],
            'coordinate_x',
            'coordinate_y',
        ],
    ]) ?>

</div>
