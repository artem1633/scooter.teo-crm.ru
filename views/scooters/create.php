<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Scooters */

?>
<div class="scooters-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
