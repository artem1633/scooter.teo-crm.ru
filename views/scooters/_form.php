<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Scooters */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="scooters-form padding_in_form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'qr_code')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'station_id')->dropDownList($model->getStationList(),['prompt' => 'Выберите']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'client_id')->dropDownList($model->getClientsList(),['prompt' => 'Выберите']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'status')->dropDownList($model->getStatusList(),[]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'run')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'state')->dropDownList($model->getStateList(),[]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h3>GPS координаты</h3>
        </div>
        <hr>
        <div class="col-md-6">
            <?= $form->field($model, 'coordinate_x')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'coordinate_y')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
