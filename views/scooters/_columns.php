<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Scooters;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    /*[
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],*/
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'id',
        'width' => '50px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'qr_code',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'station_id',
        'filter' => Scooters::getStationList(),
        'content' => function($data){
            return $data->getStationList()[$data->station_id];
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'filter' => Scooters::getStatusList(),
        'content' => function ($data) {
            return $data->getStatusDescription();
            /*return Html::dropDownList(
                'data', 
                $data->status, 
                $data->getStatusList(), 
                [
                    'onchange'=>"
                        $.get('/scooters/edit-status', {'id':$data->id, 'value':$(this).val()}, function(data){} ); 
                    ",
                    'class' =>'form-control',
                    //'style'=>'width:70px;'
                ]
            );*/
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'run',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'state',
        'filter' => Scooters::getStateList(),
        'content' => function($data){
            //return $data->getStateDescription();
            return Html::dropDownList(
                'data', 
                $data->state, 
                $data->getStateList(), 
                [
                    'onchange'=>"
                        $.get('/scooters/edit-state', {'id':$data->id, 'value':$(this).val()}, function(data){} ); 
                    ",
                    'class' =>'form-control',
                ]
            );
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'client_id',
        'filter' => Scooters::getClientsList(),
        'content' => function($data){
            return $data->getClientsList()[$data->client_id];
        }
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'coordinate_x',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'coordinate_y',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'], 
    ],

];   