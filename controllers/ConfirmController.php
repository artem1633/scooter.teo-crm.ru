<?php

namespace app\controllers;

use Yii;
use app\models\Clients;
use \yii\web\Response;
use app\models\Settings;

class ConfirmController extends \yii\web\Controller
{
    public function actionIndex($token)
    {
    	Yii::$app->user->logout();
    	$client_id = null;
    	$clients = Clients::find()->all();
    	foreach ($clients as $value) {
    		if(md5($value->id) == $token) $client_id = $value->id;
    	}
    	$client = Clients::findOne($client_id);

    	if($client === null) {
    		return $this->render('error', [
	        ]); 
    	} else {
	        return $this->render('index', [
	            'model' => $client,
	        ]);    		
    	}
    }

    public function actionCheck($id)
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = Clients::findOne($id);
            $model->status = Clients::STATUS_ACCESS;
            $model->datetime_agree = date('Y-m-d H:i:s');
            $model->agree = 1;
            $model->ip = $request->userIP;
            $model->model = (string)Yii::$app->request->userAgent;

            $model->save();
            return [
                'forceReload'=>'#personal-pjax',
                'title'=> "Соглашения",
                'size' => 'normal',
                'content' => '<span style="color:red;"> Успешно выполнено </span>',
            ];
        }
    }

    public function actionPolicy()
    {
        $setting = Settings::find()->where(['key' => 'policy'])->one();
        return $this->render('policy', [
            'setting' => $setting,
        ]);
    }

}
