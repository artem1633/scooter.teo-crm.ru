<?php

namespace app\controllers;

use Yii;
use app\models\Clients;
use app\models\ClientsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use app\models\Rents;
use app\models\RentPenalty;
use app\models\Settings;

/**
 * ClientsController implements the CRUD actions for Clients model.
 */
class ClientsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Clients models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new ClientsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Clients model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $query = Rents::find()->where(['client_id' => $id]);
        $historyProvider = new ActiveDataProvider(['query' => $query,]);
        $query = RentPenalty::find()->joinWith('rent')->where(['rents.client_id' => $id]);
        $penaltyProvider = new ActiveDataProvider(['query' => $query,]);
        $query = Rents::find()->where(['client_id' => $id, 'payment_status' => 1]);
        $payedProvider = new ActiveDataProvider(['query' => $query,]);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Клиент",
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                        'payedProvider' => $payedProvider,
                        'historyProvider' => $historyProvider,
                        'penaltyProvider' => $penaltyProvider,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $model,
                'payedProvider' => $payedProvider,
                'historyProvider' => $historyProvider,
                'penaltyProvider' => $penaltyProvider,
            ]);
        }
    }

    public function actionViewCart($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = $this->findModel($id);
            $query = Rents::find()->where(['client_id' => $id]);
            $historyProvider = new ActiveDataProvider(['query' => $query,]);
            $query = RentPenalty::find()->joinWith('rent')->where(['rents.client_id' => $id]);
            $penaltyProvider = new ActiveDataProvider(['query' => $query,]);
            $query = Rents::find()->where(['client_id' => $id, 'payment_status' => 1]);
            $payedProvider = new ActiveDataProvider(['query' => $query,]);
            
            return [
                'title'=> "Клиент",
                'content'=>$this->renderAjax('view-cart', [
                    'model' => $model,
                    'payedProvider' => $payedProvider,
                    'historyProvider' => $historyProvider,
                    'penaltyProvider' => $penaltyProvider,
                ]),
            ];    
        }
    }

    /*public function actionA()
    {
        $request = Yii::$app->request;
        $telephone = '79612206659';
        $model = new Clients();
        $model->surname = 'Ковальский';
        $model->name = 'Артем';
        $model->middle_name = 'Александрович';
        $fio = $model->surname.' '.$model->name.' '.$model->middle_name;
        $link = "http://" . $_SERVER['SERVER_NAME'] . '/confirm/index?token=' . md5(1);
        //$text = "Уважаемый(Уважаемая) {$fio}. От нашего сайта " . "http://" . $_SERVER['SERVER_NAME'] . " создан ссылка для подтверждения сведения про вас. Через этой ссылкой {$link} войдите на сайт, подтвердите свою личность и принимайте пользовательского соглашения.";
        $text = 'Privet';
        $r = $model->sendSms($telephone, $text);
        echo "<pre>";
        print_r($r);
        echo "</pre>";
    }*/


    /**
     * Creates a new Clients model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Clients();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                $model->upload();
                $telephone = $model->getPhone($model->phone);
                $setting = Settings::find()->where(['key' => 'client_sms'])->one();
                $text = $setting->text;
                $link = "http://" . $_SERVER['SERVER_NAME'] . '/confirm/index?token=' . md5($model->id);
                /*$text = "Уважаемый(Уважаемая) {$model->surname} {$model->name} {$model->middle_name}. От нашего сайта " . "http://" . $_SERVER['SERVER_NAME'] . " создан ссылка для подтверждения сведения про вас. Через этой ссылкой {$link} войдите на сайт, подтвердите свою личность и принимайте пользовательского соглашения.";*/
                $text = str_replace('{client_fio}', $model->surname . ' ' . $model->name . ' ' . $model->middle_name, $text);
                $text = str_replace('{site_name}', "http://" . $_SERVER['SERVER_NAME'], $text);
                $text = str_replace('{link}', $link, $text);
                //$text = 'Тестовый смс';
                $model->sendSms($telephone, $text);
                return $this->redirect(['view', 'id' => $model->id]);
            }else{           
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Clients model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                $model->upload();

                return [
                    'forceReload'=>'#personal-pjax',
                    'title'=> "Клиент",
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Clients model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Clients model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Clients model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Clients the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Clients::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSendSms($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);    
        $login = ConstantValues::find()->where(['key' => 'sms_login'])->one()->value;   
        $password = ConstantValues::find()->where(['key' => 'sms_password'])->one()->value;   
        Yii::$app->response->format = Response::FORMAT_JSON;

        $text = Template::find()->where(['key' => 'template_send_sms'])->one()->text_tag;
        $text = str_replace ("#КОД#", str_pad(rand(0, 99999), 4, '0', STR_PAD_LEFT), $text);
        $api = 'D54A55D7-B8FA-3CF9-4A8A-94BE6940FEE8';
        /*$body = file_get_contents("https://sms.ru/sms/send?api_id=".$api."&to=".$model->telephone."&msg=".urlencode(iconv("windows-1251","utf-8",$text))."&json=1"); # Если приходят крякозябры, то уберите iconv и оставьте только urlencode("Привет!")

        $json = json_decode($body);
        print_r($json); // Для дебага*/
        // Для разбора $json можно использовать кусок кода из предыдущего примера.
        
        $ch = curl_init("https://sms.ru/sms/send");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
            "api_id" => $api ,
            "to" => $model->telephone, // До 100 штук до раз
            "msg" => $text, // Если приходят крякозябры, то уберите iconv и оставьте только "Привет!",
            /*
            // Если вы хотите отправлять разные тексты на разные номера, воспользуйтесь этим кодом. В этом случае to и msg нужно убрать.
            "multi" => array( // до 100 штук за раз
                "79117851101"=> iconv("windows-1251", "utf-8", "Привет 1"), // Если приходят крякозябры, то уберите iconv и оставьте только "Привет!",
                "74993221627"=> iconv("windows-1251", "utf-8", "Привет 2") 
            ),
            */
            "json" => 1 // Для получения более развернутого ответа от сервера
        )));
        $body = curl_exec($ch);
        curl_close($ch);

        $json = json_decode($body);
        if ($json) { // Получен ответ от сервера
            //print_r($json); // Для дебага
            if ($json->status == "OK") { // Запрос выполнился
                foreach ($json->sms as $phone => $data) { // Перебираем массив СМС сообщений
                    if ($data->status == "OK") { // Сообщение отправлено
                        //echo "Сообщение на номер $phone успешно отправлено. ";
                        //echo "ID сообщения: $data->sms_id. ";
                        //echo "";
                    } else { // Ошибка в отправке
                        //echo "Сообщение на номер $phone не отправлено. ";
                        //echo "Код ошибки: $data->status_code. ";
                        //echo "Текст ошибки: $data->status_text. ";
                        //echo "";
                    }
                }
                //echo "Баланс после отправки: $json->balance руб.";
                //echo "";
            } else { // Запрос не выполнился (возможно ошибка авторизации, параметрах, итд...)
                //echo "Запрос не выполнился. ";      
                //echo "Код ошибки: $json->status_code. ";
                //echo "Текст ошибки: $json->status_text. ";
            }
        } else { 

            //echo "Запрос не выполнился. Не удалось установить связь с сервером. ";
            return [
                'forceReload'=>'#base-pjax',
                'title' => 'Отправить смс',
                'size' => 'normal',
                'content'=> '<span style="color:red;font-size:16px;"><center><b> Запрос не выполнился. Не удалось установить связь с сервером.  </b></center></span>'
            ];    

        }

        return [
            'forceReload'=>'#base-pjax',
            'title' => 'Отправить смс',
            'size' => 'normal',
            'content'=> '<span style="color:red;font-size:16px;"><center><b> Успешно отправлено </b></center></span>'
        ];    
    }
}
