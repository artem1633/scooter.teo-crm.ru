<?php

namespace app\controllers;

use Yii;
use app\models\Rents;
use app\models\RentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Clients;
use yii\data\ActiveDataProvider;
use app\models\Scooters;
use app\models\RentPenalty;
use app\models\Penalty;
use yii\web\HttpException;

/**
 * RentsController implements the CRUD actions for Rents model.
 */
class RentsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /*public function actionData()
    {
        $week = \app\models\WeeklyCost::find()->where(['between', 'begin_time', 'end_time', '13:30:00'])->andWhere(['week_day' => 7])->one();
        echo "<pre>";
        print_r($week);
        echo "</pre>";
        $today = date("Y-m-d");
        $number = date('N', strtotime($today));
        echo "Today: " . $today . " weekday: " . $number . "<br>";

        $today = strtotime($today);
        $tomorrow = strtotime($today);
        $tomorrow = strtotime("+1 day", $today);
        $number2 = date('N', $tomorrow);
        echo "Tomorrow: " . date('Y-m-d', $tomorrow) . " weekday: " . $number2 . "<br>";
    }*/

    public function beforeAction($action)
    {
        if(Yii::$app->user->identity->id === null) return $this->redirect(['/site/login']);
        $permission = Yii::$app->user->identity->permission;

        if(($action->id =='list') && $permission == 'manager')
        {
            throw new HttpException(403,'У вас нет разрешения на доступ к этому действию.');
        }

        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Lists all Rents models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $post = Yii::$app->request->post();
        $session = Yii::$app->session;
        $collapsed = ''; $button = 'minus';  $client = null; $client_status = 0;
        if($session['button'] == null | $session['button'] == 'plus') { $collapsed = 'collapsed-box'; $button = 'plus';}

        $model = new Rents();
        $model->begin_date = date('H:i d.m.Y');
        $model->tariff_id = 1;

        if(isset($post['phone'])){
            $client = Clients::find()->where(['like', 'phone', $post['phone']])->one();
            $model->client_id = $client->id;
            if($client !== null) {
                $rent = Rents::find()->where(['status' => 0, 'client_id' => $client->id])->one();
                if($rent !== null) $client_status = 1;
            }
        }

        if ($model->load($post) && $model->validate()) {
            
            $model->save();
            $model->setValues();
            Yii::$app->session->setFlash('success', "Успешно выполнено");
            return $this->redirect(['view', 'id' => $model->id]);
        }
        else {
            if(isset($model->client_id)){
                $client = Clients::find()->where(['id'=> $model->client_id])->one();
                $post['phone'] = $client->phone;                
            }
        }

        return $this->render('index', [
            'post' => $post,
            'client' => $client,
            'button' => $button,
            'collapsed' => $collapsed,
            'model' => $model,
            'client_status' => $client_status,
        ]);
    }

    public function actionScooterReturn($search)
    {    
        $request = Yii::$app->request;
        $penaltyProvider = null;

        $rent = Rents::find()->where(['scooter_id' => $search, 'status' => 0])->one();
        if($rent !== null){
            $query = RentPenalty::find()->where(['rent_id' => $rent->id]);
            $penaltyProvider = new ActiveDataProvider([
                'query' => $query,
            ]);                
        }

        if($request->post() && $rent->load($request->post()) && $rent->save()){
            $rent->saveValues();
            Yii::$app->session->setFlash('success', "Успешно завершено");
            return $this->redirect(['/rents/view', 'id' => $rent->id]);
        }

        return $this->render('scooter-return', [
            'post' => $request->post(),
            'rent' => $rent,
            'penaltyProvider' => $penaltyProvider,
        ]);
    }

    public function actionList()
    {
        $searchModel = new RentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearchScooter()
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->post()){
                return $this->redirect(['scooter-return', 'search' => $request->post()['scooter']]);
            }else{           
                return [
                    'title'=> "Найти самоката",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('search-scooter', []),
                    'footer'=>Html::button('Поиск',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }
        }       
    }

    /**
     * Displays a single Rents model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Информация",
                'content'=>$this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
            ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Rents model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreatePenalty($id)
    {
        $request = Yii::$app->request;
        $model = new RentPenalty();
        $model->rent_id = $id;
        $model->cost = 0;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                $rent = Rents::findOne($model->rent_id);
                $rent->penalty_sum += $model->cost;
                $rent->save();
                return [
                    'forceReload'=>'#scooter-pjax',
                    'title'=> "Штрафы",
                    'forceClose'=>true,
                    'content'=>'<span class="text-success">Успешно выполнено</span>',        
                ];         
            }else{           
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create-penalty', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }       
    }

    /**
     * Delete an existing Rents model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeletePenalty($id)
    {
        $request = Yii::$app->request;
        $penalty = RentPenalty::findOne($id);
        $rent = Rents::findOne($penalty->rent_id);
        $rent->penalty_sum -= $penalty->cost;
        $rent->save();
        $penalty->delete();
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#scooter-pjax'];
        }
    }

    /**
     * Finds the Rents model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rents the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rents::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionScooterList($id)
    {
        $scooters = Scooters::find()->where(['station_id' => $id, 'status' => Scooters::STATUS_VACANT])->all();
        foreach ($scooters as $value) {
            echo "<option value = '".$value->id."'>".'Id = ' . $value->id. '; QR-код = '. $value->qr_code."</option>" ;
        }
    }

    public function actionPenaltyCost($id)
    {
        $penalty = Penalty::findOne($id);
        return $penalty->price;
    }

    public function actionChangeEndTime($trip_duration, $begin_date, $tariff_id)
    {
        if($trip_duration == null) return null;
        if($begin_date === null || $begin_date === '') $begin_date = date('Y-m-d H:i:s');
        //else 
        if($tariff_id == 1){
            return null;
            //return strtotime($begin_date);
            return date('H:i d.m.Y', strtotime($trip_duration . " minutes", strtotime($begin_date)));
        }
        if($tariff_id == 2){
            //return strtotime($begin_date);
            return date('H:i d.m.Y', strtotime($trip_duration . " hour", strtotime($begin_date)));
        }
    }

    public function actionEditPaymentStatus($id, $value)
    {
        $rent = Rents::findOne($id);
        $rent->payment_status = $value;
        $rent->save();
    }
}
